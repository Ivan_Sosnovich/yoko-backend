FROM node:16.19.0-alpine3.16

ENV POSTGRES_HOST=postrgres
ENV POSTGRES_PORT=5432
ENV POSTGRES_USER=YOKO
ENV POSTGRES_PASSWORD=YOKO-123
ENV POSTGRES_DATABASE=yoko_db
ENV JWT_SECRET=123YOKO321trade

WORKDIR /app

COPY package*.json ./

RUN npm install

COPY . .

RUN npm run build

CMD [ "npm", "run", "start:prod" ]