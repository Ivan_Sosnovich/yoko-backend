import axios from "axios";
import { get } from "lodash";

const _instance = axios.create();

_instance.interceptors.response.use(
  function ({ data }) {
    // Any status code that lie within the range of 2xx cause this function to trigger
    // Do something with response data
    return data.response;
  },
  function (error) {
    // Any status codes that falls outside the range of 2xx cause this function to trigger
    // Do something with response error
    return Promise.reject(get(error, "response.data.error"));
  }
);

export default _instance;
