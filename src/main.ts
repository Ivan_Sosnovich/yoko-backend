import * as cookieParser from "cookie-parser";
import { SwaggerModule, DocumentBuilder } from "@nestjs/swagger";
import * as dotenv from "dotenv";
import { NestFactory } from "@nestjs/core";
import { AppModule } from "./app.module";
import { ValidationPipe } from "@nestjs/common";

dotenv.config({ path: "/.env" });

export const PROJECT_VERSION = "1.0.0";

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const config = new DocumentBuilder()
    .setTitle("Yoko Trade")
    .setDescription("Описание запросов для всего и всяга_)")
    .setVersion(PROJECT_VERSION)
    .addTag("YOKO")
    .build();
  app.useGlobalPipes(new ValidationPipe());
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup("api/swagger", app, document);
  const port = process.env.PORT || 3001;
  app.enableCors({ credentials: true, origin: true });
  app.setGlobalPrefix("api");
  app.use(cookieParser());
  await app.listen(port, () => console.log("server start at port:", port));
}
bootstrap();
