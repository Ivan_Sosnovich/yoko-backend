import { createHmac } from "crypto";

const secretKey = process.env.BOT_SECRET_KEY;

export const keyEncryptionByCrypto = (value: string) => {
  return Buffer.from(value + secretKey).toString("base64");
};

export const getCode = (value: string) => {
  return createHmac("SHA256", value).update(secretKey).digest("hex");
};
