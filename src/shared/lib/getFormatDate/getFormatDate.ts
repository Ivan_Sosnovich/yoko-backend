export const getFormatDate = (date: Date) => {
  return new Date(date.toLocaleString("ru-GB", { timeZone: "UTC" }));
};
