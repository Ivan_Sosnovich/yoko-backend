import { IPosition } from "../../../modules/position/position.types";

export type ProfitPositionProps = Pick<
  IPosition,
  "size" | "openingPrice" | "closingPrice"
>;
export const calculateProfitForPosition = (
  props: ProfitPositionProps
): number => {
  const { size, openingPrice, closingPrice } = props;
  const profit = closingPrice * size - openingPrice * size;
  return profit * 0.001;
};

//  ((closingPrice * size) - (openingPrice * size) - 0.1%комиссия биржи - 0.2 (we))
