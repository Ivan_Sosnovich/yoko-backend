import { ApiProperty } from "@nestjs/swagger";
import { HttpStatus } from "./http_status";

export class ResponseError {
  @ApiProperty()
  status?: HttpStatus;
  @ApiProperty()
  message: string;
}

export class ResponseSuccess {
  @ApiProperty()
  message: string;
}
