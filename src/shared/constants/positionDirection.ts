export enum PositionDirection {
  USER = "USER",
  SELL = "SELL",
  BUY = "BUY",
}
