import { Module } from "@nestjs/common";
import { ConfigModule } from "@nestjs/config";
import { DatabaseModule } from "./database/database.module";
import { UsersModule } from "./modules/user/users.module";
import { BotModule } from "./modules/bot/bot.module";
import { PositionsModule } from "./modules/position/positions.module";
import { TradingMachineModule } from "./modules/tradingMachine/tradingMachine.module";
import { PaymentModule } from "./modules/payment/payment.module";
import { MailModule } from "./modules/mailer/mail.module";

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      envFilePath: ".env",
    }),
    DatabaseModule,
    UsersModule,
    BotModule,
    PositionsModule,
    TradingMachineModule,
    PaymentModule,
    MailModule,
  ],
  controllers: [],
  providers: [],
  exports: [],
})
export class AppModule {}
