import { Module, forwardRef } from "@nestjs/common";
import { JwtModule } from "@nestjs/jwt";
import { DatabaseModule } from "../../database/database.module";
import { TradingMachineService } from "./tradingMachine.service";
import { TradingMachineController } from "./tradingMachine.controller";
import { BotModule } from "../bot/bot.module";
import { UsersModule } from "../user/users.module";
import { PositionsModule } from "../position/positions.module";

@Module({
  imports: [
    JwtModule.register({
      secret: process.env.JWT_SECRET,
      signOptions: { expiresIn: "2d" },
    }),
    DatabaseModule,
    forwardRef(() => BotModule),
    forwardRef(() => UsersModule),
    forwardRef(() => PositionsModule),
  ],
  providers: [TradingMachineService],
  controllers: [TradingMachineController],
  exports: [TradingMachineService],
})
export class TradingMachineModule {}
