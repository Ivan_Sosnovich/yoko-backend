import {
  Controller,
  Post,
  Body,
  UseGuards,
  Patch,
  Req,
  Res,
  Get,
} from "@nestjs/common";
import { ApiTags, ApiBody, ApiResponse, ApiHeader } from "@nestjs/swagger";
import { Request, Response } from "express";
import {
  ChangeBotStatusDto,
  ChangeUserTradingBalanceDTO,
  LoginBotDto,
} from "./dto";
import { TradingMachineService } from "./tradingMachine.service";
import {
  ResponseError,
  ResponseSuccess,
} from "../../shared/types/server/response_error";
import { TJwtAuthGuard } from "../../guards/tAuth";
import { HttpStatus } from "../../shared/types/server/http_status";
import { CreatePositionDTO } from "./dto/CreatePositionDTO";
import { GetBotSettingsResponceDTO } from "../bot/botSettings/dto";

@ApiTags("контроллер для взаимодейтсвия с торговым ботом")
@Controller("tradingMachine")
export class TradingMachineController {
  constructor(private machineService: TradingMachineService) {}

  // Логин бота и передача токена
  @Post("/login")
  @ApiBody({ type: LoginBotDto })
  @ApiResponse({ status: 200, description: "token" })
  @ApiResponse({
    status: 404,
    description: "Ошибки логирования бота или поиска бота в базу",
    type: ResponseError,
  })
  botLogin(@Body() loginBotDto: LoginBotDto, @Res() response: Response) {
    this.machineService
      .botLogin(loginBotDto)
      .then((token) => response.status(HttpStatus.OK).send(token))
      .catch((e) =>
        response
          .status(HttpStatus.NOT_FOUND)
          .send({ status: HttpStatus.NOT_FOUND, message: e.toString() })
      );
  }

  // изменения статуса бота
  @UseGuards(TJwtAuthGuard)
  @Patch("/status")
  @ApiHeader({
    name: "Authorization",
    required: true,
    description: "Bearer token",
  })
  @ApiBody({ type: ChangeBotStatusDto })
  @ApiResponse({ type: ResponseSuccess, status: 200 })
  @ApiResponse({
    status: 400,
    description: "Ошибки изменения статуса бота",
    type: ResponseError,
  })
  @ApiResponse({
    status: 401,
    description: "Ошибка авторизации",
    type: ResponseError,
  })
  changeBotStatus(
    @Body() changeBotStatusDto: ChangeBotStatusDto,
    @Req() request: Request,
    @Res() response: Response
  ) {
    const token = request.headers.authorization.split(" ")[1];
    this.machineService
      .changeBotStatus(token, changeBotStatusDto)
      .then(() => response.status(200).send({ message: "success" }))
      .catch((e) =>
        response
          .status(HttpStatus.BAD_REQUEST)
          .send({ status: HttpStatus.BAD_REQUEST, message: e.toString() })
      );
  }

  // изменение торгового баланса бота
  @UseGuards(TJwtAuthGuard)
  @Post("/update/user/balance")
  @ApiHeader({
    name: "Authorization",
    required: true,
    description: "Bearer token",
  })
  @ApiBody({ type: ChangeUserTradingBalanceDTO })
  @ApiResponse({ type: ResponseSuccess, status: 200 })
  @ApiResponse({
    status: 400,
    description: "Ошибки изменения баланса пользователя",
    type: ResponseError,
  })
  @ApiResponse({
    status: 401,
    description: "Ошибка авторизации",
    type: ResponseError,
  })
  changeUserTradingBalance(
    @Body() changeUserTradingBalanceDTO: ChangeUserTradingBalanceDTO,
    @Req() request: Request,
    @Res() response: Response
  ) {
    const token = request.headers.authorization.split(" ")[1];
    this.machineService
      .changeUserTradingBalance(token, changeUserTradingBalanceDTO)
      .then(() => response.status(200).send({ message: "success" }))
      .catch((e) =>
        response
          .status(HttpStatus.BAD_REQUEST)
          .send({ status: HttpStatus.BAD_REQUEST, message: e.toString() })
      );
  }

  // запрос на создание или закрытие позиций
  @UseGuards(TJwtAuthGuard)
  @Post("/position")
  @ApiHeader({
    name: "Authorization",
    required: true,
    description: "Bearer token",
  })
  @ApiBody({ type: CreatePositionDTO })
  @ApiResponse({ type: ResponseSuccess, status: 200 })
  @ApiResponse({
    status: 400,
    description: "Ошибки операций по позициям",
    type: ResponseError,
  })
  @ApiResponse({
    status: 401,
    description: "Ошибка авторизации",
    type: ResponseError,
  })
  positionOperation(
    @Body() positionDTO: CreatePositionDTO,
    @Req() request: Request,
    @Res() response: Response
  ) {
    const token = request.headers.authorization.split(" ")[1];
    this.machineService
      .positionOperation(token, positionDTO)
      .then(() => response.status(200).send({ message: "success" }))
      .catch((e) =>
        response
          .status(HttpStatus.BAD_REQUEST)
          .send({ status: HttpStatus.BAD_REQUEST, message: e.toString() })
      );
  }

  @UseGuards(TJwtAuthGuard)
  @Get("/settings")
  @ApiResponse({ type: GetBotSettingsResponceDTO, status: 200 })
  @ApiHeader({
    name: "Authorization",
    required: true,
    description: "Bearer token",
  })
  @ApiResponse({
    status: 401,
    description: "Ошибка авторизации",
    type: ResponseError,
  })
  getBotSettings(@Req() request: Request, @Res() response: Response) {
    const token = request.headers.authorization.split(" ")[1];
    this.machineService
      .getBotSettings(token)
      .then((data) => response.status(200).send({ ...data }))
      .catch((e) =>
        response
          .status(HttpStatus.BAD_REQUEST)
          .send({ status: HttpStatus.BAD_REQUEST, message: e.toString() })
      );
  }
}
