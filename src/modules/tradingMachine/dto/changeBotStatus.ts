import { ApiProperty } from "@nestjs/swagger";
import { IsNumber, IsOptional, IsNotEmpty, IsEnum } from "class-validator";
import { BotStatus } from "../../../shared/constants/botStatus";

export class ChangeBotStatusDto {
  @ApiProperty({ required: true })
  @IsNotEmpty()
  @IsEnum(BotStatus)
  status: BotStatus;
  @ApiProperty({ required: false })
  @IsOptional()
  @IsNumber()
  error?: number;
}
