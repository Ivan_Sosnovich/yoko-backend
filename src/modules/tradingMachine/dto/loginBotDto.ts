import { ApiProperty } from "@nestjs/swagger";
import { IsString, IsNotEmpty } from "class-validator";

export class LoginBotDto {
  @ApiProperty({
    description:
      "логин бота, который был ему передан при отправки запроса на создание",
    required: true,
  })
  @IsString()
  @IsNotEmpty()
  login: string;

  @ApiProperty({
    description:
      "пароль бота, который был ему передан при отправки запроса на создание",
    required: true,
  })
  @IsString()
  @IsNotEmpty()
  password: string;
}
