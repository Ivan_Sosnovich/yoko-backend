import { ApiProperty } from "@nestjs/swagger";
import { IsNumber, IsNotEmpty } from "class-validator";

export class ChangeUserTradingBalanceDTO {
  @ApiProperty({ required: true })
  @IsNumber()
  @IsNotEmpty()
  balance: number;
  @ApiProperty({ required: false })
  coin?: string;
}
