import { ApiProperty } from "@nestjs/swagger";
import {
  IsString,
  IsNotEmpty,
  IsEnum,
  IsOptional,
  IsNumber,
} from "class-validator";
import { PositionDirection } from "../../../shared/constants/positionDirection";

export class CreatePositionDTO {
  @ApiProperty({
    description: "Название пары в формате BTC_USDT",
    required: true,
  })
  @IsString()
  @IsNotEmpty()
  symbol: string;
  @ApiProperty({
    description: "Тип позиции, создание, закрытие, закрыто пользователем",
    required: true,
    enum: [
      PositionDirection.BUY,
      PositionDirection.SELL,
      PositionDirection.USER,
    ],
  })
  @IsNotEmpty()
  @IsEnum(PositionDirection)
  direction: PositionDirection;
  @ApiProperty({
    required: false,
  })
  @IsNumber()
  @IsOptional()
  openingPrice: number;
  @ApiProperty({
    required: false,
  })
  @IsNumber()
  @IsOptional()
  closingPrice: number;
  @ApiProperty({
    required: false,
  })
  @IsNumber()
  @IsOptional()
  targetPrice: number;
  @ApiProperty({
    required: false,
  })
  @IsNumber()
  @IsOptional()
  size: number;
}
