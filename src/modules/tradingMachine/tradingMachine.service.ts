import {
  forwardRef,
  HttpException,
  HttpStatus,
  Inject,
  Injectable,
  Logger,
} from "@nestjs/common";
import { JwtService } from "@nestjs/jwt";
import {
  ChangeBotStatusDto,
  ChangeUserTradingBalanceDTO,
  LoginBotDto,
} from "./dto";
import { BotService } from "../bot/bot.service";
import { IBotJwt } from "../bot/bot.types";
import { BotStatus } from "../../shared/constants/botStatus";
import { UsersService } from "../user/users.service";
import { CreatePositionDTO } from "./dto/CreatePositionDTO";
import { PositionDirection } from "../../shared/constants/positionDirection";
import { PositionsService } from "../position/positions.service";
import { calculateProfitForPosition } from "../../shared/lib/calculateProfitForPosition";
import { IReferral } from "../user/referral/referral.types";
import { keyEncryptionByCrypto } from "../../shared/lib/keyEncryptionByCrypto";

@Injectable()
export class TradingMachineService {
  private readonly logger = new Logger(TradingMachineService.name);

  constructor(
    private jwtService: JwtService,
    @Inject(forwardRef(() => BotService)) private _botService: BotService,
    @Inject(forwardRef(() => UsersService)) private _userService: UsersService,
    @Inject(forwardRef(() => PositionsService))
    private _positionService: PositionsService
  ) {}

  async botLogin(loginBotDto: LoginBotDto) {
    this.logger.warn(
      `Бот под логиным: ${loginBotDto.login}, пытается авторизоваться`
    );
    try {
      const bot = await this._botService.botLogin(loginBotDto);
      if (bot) {
        const token: IBotJwt = {
          id: bot.id,
          userId: bot.userId,
          login: bot.login,
          password: bot.password,
        };
        return this.jwtService.sign({ ...token });
      }
    } catch (e) {
      this.logger.error(`При авторизации бота, произошла ошибка: ${e}`);
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          message: e.toString(),
        },
        HttpStatus.NOT_FOUND
      );
    }
  }

  async changeBotStatus(token: string, changeBotStatusDto: ChangeBotStatusDto) {
    try {
      const { status, error } = changeBotStatusDto;
      const { id }: IBotJwt = this.jwtService.verify(token);
      this.logger.warn(
        `Попытка изменения статуса бота по id: ${id}, на статус ${status}`
      );
      // если статус не передан, передаем ошибку
      if (!status) {
        throw Error(`Не передан новый статус бота`);
      }
      if (
        status !== BotStatus.ON &&
        status !== BotStatus.OFF &&
        status !== BotStatus.ERROR
      ) {
        throw Error(`Переданный новый статус: ${status}, не валиден`);
      }
      if (status === BotStatus.ERROR && !error) {
        throw Error(`При статусе: ${status}, поле error, обязательно`);
      }
      await this._botService.changeBotStatus(id, status, error);
    } catch (e) {
      this.logger.error(`При изменении статуса бота, произошла ошибка: ${e}`);
      throw e;
    }
  }

  async changeUserTradingBalance(
    token: string,
    changeUserTradingBalanceDTO: ChangeUserTradingBalanceDTO
  ) {
    const { userId, id }: IBotJwt = this.jwtService.verify(token);
    try {
      const { balance } = changeUserTradingBalanceDTO;
      this.logger.warn(
        `Попытка изменения торгового баланса пользователя по id: ${userId}, на ${balance}`
      );
      if (!balance) {
        throw Error(`Поле баланс не может быть ${balance}`);
      }
      if (!userId) {
        throw Error(
          `Для бота по id: ${id}, нет пользователя в токене, поле userId = ${userId}`
        );
      }
      return this._userService.changeUserTradingBalance(userId, balance);
    } catch (e) {
      this.logger.error(
        `При изменении торгового баланса пользователя :${userId}, произошла ошибка: ${e}`
      );
      throw e;
    }
  }

  async positionOperation(token: string, positionDTO: CreatePositionDTO) {
    const { userId, id }: IBotJwt = this.jwtService.verify(token);
    try {
      this.logger.warn(
        `Поступил запрос на операции по позициям для пользователя по id: ${userId}, от бота по id: ${id}`
      );
      const { direction, symbol } = positionDTO;
      const coins = symbol.split("_");
      if (coins.length !== 2) {
        throw Error(`Тип поля symbol: ${symbol}, не соотвествует требованию`);
      }
      if (direction === PositionDirection.BUY) {
        return this._openPositionForUser(userId, positionDTO);
      }
      if (direction === PositionDirection.SELL) {
        return this._closedPositionForUserByBot(userId, positionDTO);
      }
      if (direction === PositionDirection.USER) {
        return this._closedPositionForUserByUser(userId, positionDTO);
      }
    } catch (e) {
      this.logger.error(
        `При операции по позиции произошла ошибка: ${e.toString()}, по id бота: ${id}, для пользователя по id: ${userId}`
      );
      throw e;
    }
  }

  async getBotSettings(token: string) {
    try {
      const { id }: IBotJwt = this.jwtService.verify(token);
      this.logger.log(`Бот запросил свои найтройки, по id: ${id}`);
      const bot = await this._botService.getBotDataForID(id);
      const botSettings = await this._botService.getBotSettings(id);
      const hashApiKey = keyEncryptionByCrypto(botSettings.apiKey);
      const hashSecretKey = keyEncryptionByCrypto(botSettings.secretKey);
      return {
        ...botSettings,
        apiKey: hashApiKey,
        secretKey: hashSecretKey,
        status: bot.status,
      };
    } catch (e) {
      throw e.toString();
    }
  }

  // метод открытия позиции когда direction = BUY
  async _openPositionForUser(userId: number, positionDTO: CreatePositionDTO) {
    const { symbol, openingPrice, targetPrice, size } = positionDTO;
    try {
      this.logger.warn(
        `Открытие позиции по монете: ${symbol}, для пользователя: ${userId}`
      );
      if (!openingPrice) {
        throw Error("Для открытия позиции поле: openingPrice - обязательно");
      }
      if (!targetPrice) {
        throw Error("Для открытия позиции поле: targetPrice - обязательно");
      }
      if (!size) {
        throw Error("Для открытия позиции поле: size - обязательно");
      }
      this.logger.log(
        `Ищем главную открытую позицию по монете: ${symbol}, для пользователя id: ${userId}`
      );
      const parentPositionForCoin =
        await this._positionService.searchParentPositionForCoin(symbol, userId);
      if (parentPositionForCoin) {
        this.logger.log(`В базе нашлась открытая позиция по монете: ${symbol}`);
        this.logger.log(
          `Делаем позицию по id: ${parentPositionForCoin.id}, главной позицией`
        );
        this.logger.log(
          `Создаем новую позицию, привязываем к ней id родительской позиции`
        );
        await this._positionService.createNewPosition({
          userId,
          ...positionDTO,
          isOpen: true,
          isParentPositions: false,
          parentPositionId: parentPositionForCoin.id,
        });
      }
      if (!parentPositionForCoin) {
        this.logger.log(
          `В базе не нашлось открытой позиции по монете: ${symbol}, просто создаем новую позицию`
        );
        await this._positionService.createNewPosition({
          userId,
          ...positionDTO,
          isOpen: true,
          isParentPositions: true,
        });
      }
    } catch (e) {
      throw e.toString();
    }
  }

  // метод закрытия позиции ботом когда direction = SELL
  async _closedPositionForUserByBot(
    userId: number,
    positionDTO: CreatePositionDTO
  ) {
    const { symbol, closingPrice, targetPrice } = positionDTO;
    let profit = 0;
    let commissions = 0;
    try {
      this.logger.log(
        `Поступил запрос на закрытия всех позиций по монете: ${symbol}, для пользователя: ${userId}`
      );
      this.logger.log(
        `Находим все открытые позиции по монете: ${symbol}, для пользователя id: ${userId}`
      );
      const positionsForCoin =
        await this._positionService.searchOpenPositionsForCoin(symbol, userId);
      if (!positionsForCoin.length) {
        throw `нет открытых позиций по монете: ${symbol}`;
      }
      // если в массиве больше чем одна позиция
      if (positionsForCoin.length > 1) {
        // обновляем все позиции по результату продажи
        for (const position of positionsForCoin) {
          const positionsProfit = calculateProfitForPosition({
            size: position.size,
            openingPrice: position.openingPrice,
            closingPrice,
          });
          await this._positionService.changePosition(position.id, {
            closingPrice,
            targetPrice,
            isOpen: false,
            profit: positionsProfit,
          });
          // сразу высчитываем профит по каждой позиции
          profit += positionsProfit;
        }
        commissions = profit * 0.2;
      }
      // если в массиве одна позиция
      if (positionsForCoin.length === 1) {
        const [position] = positionsForCoin;
        const positionProfit = calculateProfitForPosition({
          size: position.size,
          openingPrice: position.openingPrice,
          closingPrice,
        });
        commissions = positionProfit * 0.2;
      }
      // списываем коммисию с баланса пользователя
      await this._userService.debitingBalanceForUser(userId, commissions);
      // отправляем запрос на пополнение реф баланса пользователя, если таковой имеется
      const referralProfit = commissions * 0.3;
      const referralUser =
        await this._userService.updateReferralUserBalanceForUser(
          userId,
          referralProfit
        );
      if (referralUser) {
        this.logger.warn(
          `Найден реферал: ${referralUser.id}, баланс обновлен, создаем реф дату в таблице рефералов`
        );
        const referralData: Omit<IReferral, "date"> = {
          profit: referralProfit,
          userId: referralUser.id,
          referralUser: referralUser,
        };
        await this._userService.createReferralForUser(userId, referralData);
      }
    } catch (e) {
      this.logger.error(
        `При закрытии позиции по монете: ${symbol}, произошла ошибка: ${e}`
      );
      throw e.toString();
    }
  }

  // метод закрытия позиций пользователем когда direction = USER
  async _closedPositionForUserByUser(
    userId: number,
    positionDTO: CreatePositionDTO
  ) {
    try {
      const { symbol, direction } = positionDTO;
      this.logger.warn(
        `Пользователь id: ${userId} сам закрыл сделки по манете: ${symbol}`
      );
      // находим все открытые сделки по монете
      const positionsForCoin =
        await this._positionService.searchOpenPositionsForCoin(symbol, userId);
      // обновляем все позиции по результату продажи
      for (const position of positionsForCoin) {
        await this._positionService.changePosition(position.id, {
          closingPrice: 0,
          targetPrice: 0,
          isOpen: false,
          profit: 0,
          direction,
          commissions: 0,
        });
      }
    } catch (e) {
      throw e.toString();
    }
  }

  async _validateBot(token: string) {
    try {
      const { login, password }: IBotJwt = this.jwtService.verify(token);
      const newToken = await this._botService.botLogin({ login, password });
      return !!newToken;
    } catch (e) {
      this.logger.error(`Валидация бота завершилась ошибкой: ${e.toString()}`);
    }
  }
}
