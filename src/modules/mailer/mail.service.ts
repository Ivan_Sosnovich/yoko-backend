import { MailerService } from "@nestjs-modules/mailer";
import { Injectable, Logger } from "@nestjs/common";

@Injectable()
export class MailService {
  private readonly logger = new Logger(MailService.name);

  constructor(private mailerService: MailerService) {}

  async sendUserConfirmation(email: string, url: string) {
    try {
      this.logger.warn(
        `Отправка письма верификации на email: ${email}, с url: ${url}`
      );
      return await this.mailerService.sendMail({
        to: email,
        subject: "Welcome YOKO TRADE",
        template: "./confirmation",
        context: {
          email,
          url,
        },
      });
    } catch (e) {
      throw e.toString();
    }
  }
  async sendEmailForPasswordRecovery(email: string, url: string) {
    try {
      this.logger.warn(
        `Отправка письма для сброса пароля на email: ${email}, с url: ${url}`
      );
      return await this.mailerService.sendMail({
        to: email,
        subject: "Recovery password for YOKO TRADE",
        template: "./recovery",
        context: {
          email,
          url,
        },
      });
    } catch (e) {
      throw e.toString();
    }
  }
}
