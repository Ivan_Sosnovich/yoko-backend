import { Module, forwardRef } from "@nestjs/common";
import { PaymentController } from "./payment.controller";
import { DatabaseModule } from "../../database/database.module";
import { depositProviders } from "./deposit/deposit.providers";
import { DepositService } from "./deposit/deposit.service";
import { DepositRepository } from "./deposit/deposit.repository";
import { transactionsProviders } from "./deposit/transactions/transactions.providers";
import { TransactionsService } from "./deposit/transactions/transactions.service";
import { TransactionsRepository } from "./deposit/transactions/transactions.repository";
import { UsersModule } from "../user/users.module";
@Module({
  imports: [DatabaseModule, forwardRef(() => UsersModule)],
  providers: [
    ...depositProviders,
    DepositService,
    DepositRepository,
    ...transactionsProviders,
    TransactionsService,
    TransactionsRepository,
  ],
  controllers: [PaymentController],
  exports: [DepositService],
})
export class PaymentModule {}
