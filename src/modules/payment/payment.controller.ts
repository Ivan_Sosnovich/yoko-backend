import {
  Controller,
  UseGuards,
  Post,
  Req,
  Res,
  Body,
  Param,
} from "@nestjs/common";
import { ApiTags, ApiBody, ApiResponse, ApiHeader } from "@nestjs/swagger";
import { Request, Response } from "express";
import { DepositService } from "./deposit/deposit.service";
import { JwtAuthGuard } from "../../guards/JwtGuard";
import { CreateOrderDTO, CreateOrderResponseDTO } from "./deposit/dto";
import { ResponseError } from "../../shared/types/server/response_error";
import { IUser } from "../user/user.types";
import { HttpStatus } from "../../shared/types/server/http_status";
import { IUpdateOrderRequest } from "./deposit/deposit.types";

@ApiTags("payment controller")
@Controller("payment")
export class PaymentController {
  constructor(private _depositService: DepositService) {}

  @UseGuards(JwtAuthGuard)
  @Post("/order/create")
  @ApiBody({ type: CreateOrderDTO })
  @ApiHeader({
    name: "Authorization",
    required: true,
    description: "Bearer token",
  })
  @ApiResponse({ status: 200, type: CreateOrderResponseDTO })
  @ApiResponse({
    status: 401,
    description: "Ошибка авторизации",
    type: ResponseError,
  })
  @ApiResponse({
    status: 404,
    description: "Ошибки перевода с реф баланса на баланс пользователя",
    type: ResponseError,
  })
  createOrder(
    @Body() createOrderDTO: CreateOrderDTO,
    @Req() request: Request,
    @Res() response: Response
  ) {
    const { id } = request.user as IUser;
    const { deposit } = createOrderDTO;
    this._depositService
      .createOrder(id, deposit)
      .then((data) => response.status(200).send(data))
      .catch((e) =>
        response
          .status(HttpStatus.BAD_REQUEST)
          .send({ status: HttpStatus.BAD_REQUEST, message: e.toString() })
      );
  }

  @Post("/order/update/:userId/:orderId")
  async updateOrder(
    @Param() { userId, orderId },
    @Body() body: IUpdateOrderRequest,
    @Res() response: Response
  ) {
    this._depositService
      .updateOrder(userId, orderId, body)
      .then((data) => response.status(200).send(data))
      .catch((e) =>
        response
          .status(HttpStatus.BAD_REQUEST)
          .send({ status: HttpStatus.BAD_REQUEST, message: e.toString() })
      );
  }
}
