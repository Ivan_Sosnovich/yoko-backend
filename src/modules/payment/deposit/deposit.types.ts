import { UserEntity } from "../../user/user.entity";
import { ITransactions } from "./transactions/transactions.types";

export type DepositStatus =
  | "init"
  | "error"
  | "processed"
  | "pending"
  | "expired"
  | "partial";

export type IDeposit = {
  id?: number;
  userId: UserEntity;
  status: DepositStatus;
  amount: string;
  createAt: Date;
  closedAt: Date;
  description?: string;
  network: string;
  currency: string;
  advancedBalanceId: string;
  address?: string;
  received?: string;
  transactionsIds?: number[];
  paymentOrderID?: string;
  addressId?: string;
  link?: string;
  clientOrderId?: string;
  expiresAt?: string;
};

export type IOrderCreateResponse = {
  orderId: string;
  addressId: string;
  status: string;
  address: string;
  amount: string;
  createdAt: Date;
  link: string;
  clientOrderId: string;
  expiresAt: string;
};

export type IUpdateOrderRequest = {
  status: DepositStatus;
  received: string;
  transactions?: ITransactions[];
};
