import {
  Column,
  Entity,
  JoinColumn,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
} from "typeorm";
import { UserEntity } from "../../user/user.entity";
import { TransactionsEntity } from "./transactions/transactions.entity";
import { DEPOSIT_TABLE_NAME } from "../../../shared/constants/tableName";
import { IDeposit, DepositStatus } from "./deposit.types";

@Entity(DEPOSIT_TABLE_NAME)
export class DepositEntity implements IDeposit {
  @PrimaryGeneratedColumn("increment")
  id: number;

  @OneToOne(() => UserEntity, (user) => user.id)
  @JoinColumn({ name: "userId" })
  userId: UserEntity;

  @Column({ type: "text" })
  status: DepositStatus;

  @Column({ type: "text" })
  amount: string;

  @CreateDateColumn({ name: "createAt" })
  createAt: Date;

  @UpdateDateColumn({ name: "closedAt" })
  closedAt: Date;

  @Column({ type: "text", nullable: true })
  description: string;

  @Column({ type: "text" })
  network: string;

  @Column({ type: "text", nullable: true })
  currency: string;

  @Column({ type: "text" })
  advancedBalanceId: string;

  @Column({ type: "text", nullable: true })
  address: string;

  @Column({ type: "text", nullable: true })
  received: string;

  @OneToMany(() => TransactionsEntity, (transactions) => transactions.orderId, {
    nullable: true,
    cascade: true,
    eager: true,
  })
  @JoinColumn({ name: "transactionsIds" })
  transactionsIds: number[];

  @Column({ type: "text", nullable: true })
  paymentOrderID: string;

  @Column({ type: "text", nullable: true })
  addressId: string;

  @Column({ type: "text", nullable: true })
  link: string;

  @Column({ type: "text", nullable: true })
  clientOrderId: string;

  @Column({ type: "text", nullable: true })
  expiresAt: string;
}
