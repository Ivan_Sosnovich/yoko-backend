import { Inject, Injectable } from "@nestjs/common";
import { DEPOSIT_TABLE_NAME } from "../../../shared/constants/tableName";
import { Repository } from "typeorm";
import { DepositEntity } from "./deposit.entity";
import { IDeposit } from "./deposit.types";
import { UserEntity } from "../../user/user.entity";

@Injectable()
export class DepositRepository {
  constructor(
    @Inject(DEPOSIT_TABLE_NAME)
    private _depositRepository: Repository<DepositEntity>
  ) {}

  public createOrder(deposit: Omit<IDeposit, "createAt" | "closedAt">) {
    return this._depositRepository.save({
      ...new DepositEntity(),
      ...deposit,
    });
  }

  public updateOrder(updateOrderData: Partial<IDeposit>) {
    return this._depositRepository.update(updateOrderData.id, updateOrderData);
  }

  public searchOrderForId(id: number) {
    return this._depositRepository.findOneBy({ id });
  }

  public getDepositUserData(
    user: UserEntity,
    count: number,
    pageNumber: number
  ) {
    return this._depositRepository.findAndCount({
      where: {
        userId: user,
      },
      take: count,
      skip: count * pageNumber,
      order: {
        createAt: "DESC",
      },
    });
  }
}
