import { Inject, Injectable } from "@nestjs/common";
import { TRANSACTIONS_TABLE_NAME } from "../../../../shared/constants/tableName";
import { Repository } from "typeorm";
import { TransactionsEntity } from "./transactions.entity";
import { ITransactions } from "./transactions.types";

@Injectable()
export class TransactionsRepository {
  constructor(
    @Inject(TRANSACTIONS_TABLE_NAME)
    private _transactionsRepository: Repository<TransactionsEntity>
  ) {}

  public createTransactions(transactions: ITransactions) {
    return this._transactionsRepository.save({
      ...new TransactionsEntity(),
      ...transactions,
    });
  }
}
