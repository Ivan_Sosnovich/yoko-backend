import { DepositEntity } from "../deposit.entity";

export interface ITransactions {
  id?: number;
  status: string;
  network: string;
  currency: string;
  amount: string;
  tx: string;
  sender: string;
  confirmations: string;
  orderId?: DepositEntity;
  createAt: Date;
}
