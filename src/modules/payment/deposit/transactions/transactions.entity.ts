import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  CreateDateColumn,
} from "typeorm";
import { DepositEntity } from "../deposit.entity";
import { TRANSACTIONS_TABLE_NAME } from "../../../../shared/constants/tableName";
import { ITransactions } from "./transactions.types";

@Entity(TRANSACTIONS_TABLE_NAME)
export class TransactionsEntity implements ITransactions {
  @PrimaryGeneratedColumn("increment")
  id: number;

  @Column({ type: "text" })
  amount: string;

  @Column({ type: "text" })
  confirmations: string;

  @Column({ type: "text" })
  currency: string;

  @Column({ type: "text" })
  network: string;

  @Column({ type: "text" })
  sender: string;

  @Column({ type: "text" })
  status: string;

  @Column({ type: "text" })
  tx: string;

  @CreateDateColumn({ name: "createAt" })
  createAt: Date;

  @ManyToOne(() => DepositEntity, (payment) => payment.id)
  @JoinColumn({ name: "orderId" })
  orderId: DepositEntity;
}
