import { Injectable, Logger } from "@nestjs/common";
import { TransactionsRepository } from "./transactions.repository";
import { ITransactions } from "./transactions.types";
import { DepositEntity } from "../deposit.entity";
import { TransactionsEntity } from "./transactions.entity";
import { getNewTransactionsData } from "./lib/getNewTransactionsData";
import { getFormatDate } from "src/shared/lib/getFormatDate";

@Injectable()
export class TransactionsService {
  private readonly logger = new Logger(TransactionsService.name);

  constructor(private _transactionsRepository: TransactionsRepository) {}

  async createTransactions(
    transactions: ITransactions[],
    order: DepositEntity
  ) {
    try {
      this.logger.warn(
        `Поступил запрос на создание транзакций для оредра id: ${order.id}`
      );
      for (const transaction of transactions) {
        const newTransaction = {
          ...new TransactionsEntity(),
          ...getNewTransactionsData(transaction),
          orderId: order,
        };
        await this._transactionsRepository.createTransactions(newTransaction);
      }
    } catch (e) {
      throw e.toString();
    }
  }
}
