import { DATABASE_CONNECTION } from "../../../../database/database.providers";
import { AppDataSource } from "../../../../database/data-source";
import { TransactionsEntity } from "./transactions.entity";
import { TRANSACTIONS_TABLE_NAME } from "../../../../shared/constants/tableName";

export const transactionsProviders = [
  {
    provide: TRANSACTIONS_TABLE_NAME,
    useFactory: () => AppDataSource.getRepository(TransactionsEntity),
    inject: [DATABASE_CONNECTION],
  },
];
