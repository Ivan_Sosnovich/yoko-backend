import { ITransactions } from "../../transactions.types";

export const getNewTransactionsData = (
  transaction: ITransactions
): Omit<ITransactions, "createAt"> => {
  return {
    status: transaction.status,
    network: transaction.network,
    currency: transaction.currency,
    amount: transaction.amount,
    tx: transaction.tx,
    sender: transaction.sender,
    confirmations: transaction.confirmations,
  };
};
