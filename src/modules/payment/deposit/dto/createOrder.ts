import { ApiProperty } from "@nestjs/swagger";

export class CreateOrderDTO {
  @ApiProperty({ required: true })
  deposit: number;
}
