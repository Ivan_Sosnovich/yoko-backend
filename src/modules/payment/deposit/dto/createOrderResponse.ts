import { ApiProperty } from "@nestjs/swagger";

export class CreateOrderResponseDTO {
  @ApiProperty({ required: true })
  link: string;
}
