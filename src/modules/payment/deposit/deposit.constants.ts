export const DEPOSIT_PATH = "https://ocp.onchainpay.io/api-gateway/make-order";
export const WITHDRAWAL_PATH =
  "https://ocp.onchainpay.io/api-gateway/make-withdrawal";

export const PAYMENT_WEBHOOK = `${process.env.API_SERVER_HOST}/payment/order/update`;
