import { forwardRef, Inject, Injectable, Logger } from "@nestjs/common";
import { DepositRepository } from "./deposit.repository";
import { UsersService } from "../../user/users.service";
import { getOrderData } from "./lib/getOrderData";
import { getDepositBody } from "./lib/getDepositBody";
import { DEPOSIT_PATH } from "./deposit.constants";
import { createHmac } from "crypto";
import http from "../../../api/_axios";
import { IOrderCreateResponse, IUpdateOrderRequest } from "./deposit.types";
import { getNewOrderData } from "./lib/getNewOrderData";
import { TransactionsService } from "./transactions/transactions.service";

@Injectable()
export class DepositService {
  private readonly logger = new Logger(DepositService.name);

  constructor(
    private _depositRepository: DepositRepository,
    @Inject(forwardRef(() => UsersService)) private _userService: UsersService,
    private _transactionsService: TransactionsService
  ) {}

  async createOrder(userId: number, deposit: number) {
    try {
      this.logger.warn(
        `От пользователя по id: ${userId}, поступил запрос на создание платежки на сумму: ${deposit}`
      );
      if (!deposit) {
        throw Error(`Поле deposit обязательно`);
      }
      if (deposit < 0) {
        throw Error(`Поле deposit обязательно долюно быть больше 0`);
      }
      if (deposit < 10) {
        throw Error(`Поле deposit обязательно должно быть больше 10`);
      }
      const user = await this._userService.searchUserForId(userId);
      if (!user) {
        throw Error(`Пользователь по id: ${userId}, не найден`);
      }
      const orderData = getOrderData(user, `${deposit}`);
      const newOrder = await this._depositRepository.createOrder(orderData);
      const newOrderBody = getDepositBody(`${deposit}`, userId, newOrder.id);
      const stringify = JSON.stringify(newOrderBody);
      const secretKey = createHmac("SHA256", process.env.PAYMENT_SECRET_API_KEY)
        .update(stringify)
        .digest("hex");
      const publicKey = process.env.PAYMENT_PUBLIC_KEY;
      const userOrder: IOrderCreateResponse = await http.request({
        method: "POST",
        url: DEPOSIT_PATH,
        headers: {
          "Content-type": "application/json",
          "x-api-public-key": publicKey,
          "x-api-signature": secretKey,
        },
        data: newOrderBody,
      });
      await this._depositRepository.updateOrder(
        getNewOrderData(newOrder, userOrder)
      );
      if (!userOrder.link) {
        throw Error("При формировании ордера не пришла ссылка от платежки");
      }
      return { link: userOrder.link };
    } catch (e) {
      this.logger.error(
        `При создании ордера на пополнение баланса, для пользователя по id: ${userId}, произошла ошибка: ${e.toString()}`
      );
      throw e.toString();
    }
  }

  async updateOrder(
    userId: number,
    orderId: number,
    body: IUpdateOrderRequest
  ) {
    try {
      this.logger.warn(
        `Поступила информация от платжеки для обновления ордера по id: ${orderId}, для пользователя: ${userId}`
      );
      const ordersTransactions = body.transactions;
      const userOrder = await this._depositRepository.searchOrderForId(orderId);
      if (!userOrder) {
        throw Error(`Ордер по id: ${orderId}, в базе не найден`);
      }
      await this._transactionsService.createTransactions(
        ordersTransactions,
        userOrder
      );
      await this._depositRepository.updateOrder({
        received: body.received,
        status: body.status,
        id: userOrder.id,
      });
      const orderDeposit = +body.received - +userOrder.received;
      await this._userService.depositToUserBalance(userId, orderDeposit);
    } catch (e) {
      this.logger.error(
        `При обновлении платежки от платежной системы для пользователя по id: ${userId}, по id ордера: ${orderId}, произошла ошибка: ${e.toString()}`
      );
      throw e.toString();
    }
  }

  async getDepositUserData(id: number, count: number, pageNumber: number) {
    try {
      const userBase = await this._userService.searchUserForId(id);
      // TODO - написать утилиту маппинга данных и формирования возращаемого объекта, убрать при передаче лишние данные
      const [depositData, depositCount] =
        await this._depositRepository.getDepositUserData(
          userBase,
          count,
          pageNumber
        );
      return {
        data: depositData,
        count: depositCount,
      };
    } catch (e) {
      throw e.toString();
    }
  }
}
