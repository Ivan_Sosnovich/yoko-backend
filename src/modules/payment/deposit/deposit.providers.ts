import { DATABASE_CONNECTION } from "../../../database/database.providers";
import { AppDataSource } from "../../../database/data-source";
import { DepositEntity } from "./deposit.entity";
import { DEPOSIT_TABLE_NAME } from "../../../shared/constants/tableName";

export const depositProviders = [
  {
    provide: DEPOSIT_TABLE_NAME,
    useFactory: () => AppDataSource.getRepository(DepositEntity),
    inject: [DATABASE_CONNECTION],
  },
];
