import { PAYMENT_WEBHOOK } from "../../deposit.constants";

export type DepositBody = {
  advancedBalanceId: string;
  currency: string;
  network: "bsc";
  amount: string;
  order: string;
  lifetime: number;
  description: string;
  successWebhook: string;
  errorWebhook: string;
  returnUrl: string;
  nonce: number;
};

export const getDepositBody = (
  deposit: string,
  userId: number,
  orderID: number
): DepositBody => {
  return {
    advancedBalanceId: process.env.PAYMENT_ADVVANCED_BALANCE_ID,
    currency: "USDT",
    network: "bsc",
    amount: deposit,
    order: `${orderID}`,
    lifetime: 43200,
    description: `Deposit for orderID: ${orderID}`,
    successWebhook: `${PAYMENT_WEBHOOK}/${userId}/${orderID}`,
    errorWebhook: `${PAYMENT_WEBHOOK}/${userId}`,
    returnUrl: "https://example.com/",
    nonce: Date.now(),
  };
};
