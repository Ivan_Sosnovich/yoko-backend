import { omit } from "lodash";
import { DepositEntity } from "../../deposit.entity";
import { IDeposit, IOrderCreateResponse } from "../../deposit.types";

export const getNewOrderData = (
  baseOrder: DepositEntity,
  newCreateOrder: IOrderCreateResponse
): IDeposit => {
  return {
    ...omit(baseOrder, "transactions"),
    link: newCreateOrder.link,
    addressId: newCreateOrder.addressId,
    clientOrderId: newCreateOrder.orderId,
    expiresAt: newCreateOrder.expiresAt,
  };
};
