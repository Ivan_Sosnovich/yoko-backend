import { UserEntity } from "../../../../user/user.entity";
import { IDeposit } from "../../deposit.types";

export const getOrderData = (
  userId: UserEntity,
  amount: string
): Omit<IDeposit, "createAt" | "closedAt"> => {
  return {
    userId,
    status: "init",
    amount,
    network: "bsc",
    advancedBalanceId: process.env.PAYMENT_ADVVANCED_BALANCE_ID,
    received: "0",
    currency: "USDT",
  };
};
