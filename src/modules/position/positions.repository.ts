import { Injectable, Inject } from "@nestjs/common";
import { Repository } from "typeorm";
import { POSITION_TABLE_NAME } from "../../shared/constants/tableName";
import { PositionsEntity } from "./positions.entity";
import { IPosition } from "./position.types";

@Injectable()
export class PositionsRepository {
  constructor(
    @Inject(POSITION_TABLE_NAME)
    private _positionRepository: Repository<PositionsEntity>
  ) {}

  public searchPositionForCoin(symbol: string) {
    return this._positionRepository.findOneBy({ symbol, isOpen: true });
  }

  public createNewPosition(newPosition: Omit<IPosition, "openingTime">) {
    return this._positionRepository.save({
      ...new PositionsEntity(),
      ...newPosition,
    });
  }

  public changePosition(id: number, newPositionData: Partial<IPosition>) {
    return this._positionRepository.update(id, { ...newPositionData });
  }

  public searchOpenPositionsForCoin(symbol: string, userId: number) {
    return this._positionRepository.findBy({ isOpen: true, symbol, userId });
  }

  public getPositionForYear(userId: number) {
    return this._positionRepository.findBy({ userId, isOpen: false });
  }

  public searchParentPositionForCoin(symbol: string, userId: number) {
    return this._positionRepository.findOneBy({
      symbol,
      isOpen: true,
      isParentPositions: true,
      userId,
    });
  }

  public getOpenPositionForUser(id: number) {
    return this._positionRepository.find({
      where: {
        userId: id,
        isOpen: true,
      },
      order: {
        openingTime: "DESC",
      },
    });
  }

  public getHistoryPositionForUser(id: number) {
    return this._positionRepository.find({
      where: {
        userId: id,
        isOpen: false,
      },
      order: {
        openingTime: "DESC",
      },
    });
  }
}
