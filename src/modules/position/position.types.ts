import { PositionDirection } from "../../shared/constants/positionDirection";

export interface IPosition {
  id?: number;
  symbol: string;
  isOpen: boolean;
  direction: PositionDirection;
  size: number;
  openingPrice: number;
  closingPrice?: number;
  openingTime: Date;
  closingTime?: Date;
  commissions?: number;
  targetPrice: number;
  parentPositionId?: number;
  isParentPositions: boolean;
  profit?: number;
  userId: number;
}
