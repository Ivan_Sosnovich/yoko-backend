import { Injectable, Logger } from "@nestjs/common";
import { PositionsRepository } from "./positions.repository";
import { IPosition } from "./position.types";
import { getPositionDataForYear } from "./lib/getPositionDataForYear";
import { getPositionDataForOpen } from "./lib/getPositionDataForOpen";
import { getPositionDataForHistory } from "./lib/getPositionDataForHistory";

@Injectable()
export class PositionsService {
  private readonly logger = new Logger(PositionsService.name);

  constructor(private _positionRepository: PositionsRepository) {}

  async searchPositionForCoin(coin: string) {
    try {
      return this._positionRepository.searchPositionForCoin(coin);
    } catch (e) {
      throw e.toString();
    }
  }

  async createNewPosition(newPosition: Omit<IPosition, "openingTime">) {
    try {
      this.logger.log(
        `Поступил запрос на создание позиции по монете: ${newPosition.symbol}`
      );
      return this._positionRepository.createNewPosition(newPosition);
    } catch (e) {
      this.logger.error(
        `При создании позиции по монете: ${
          newPosition.symbol
        }, произошла ошибка: ${e.toString()}`
      );
      throw e.toString();
    }
  }

  async changePosition(id: number, newPositionData: Partial<IPosition>) {
    try {
      return this._positionRepository.changePosition(id, newPositionData);
    } catch (e) {
      throw e.toString();
    }
  }

  async searchOpenPositionsForCoin(coin: string, userId: number) {
    try {
      return this._positionRepository.searchOpenPositionsForCoin(coin, userId);
    } catch (e) {
      throw e.toString();
    }
  }

  async getPositionForYear(userId: number) {
    try {
      this.logger.log(
        `Поступил запрос на поиск позиций за год для пользователя по id: ${userId}`
      );
      const userPosition = await this._positionRepository.getPositionForYear(
        userId
      );
      return getPositionDataForYear(userPosition);
    } catch (e) {
      this.logger.error(
        `При поиске позиции за год для пользователя по id: ${userId}, возникла ошибка: ${e}`
      );
      throw e.toString();
    }
  }

  async searchParentPositionForCoin(coin: string, userId: number) {
    try {
      return this._positionRepository.searchParentPositionForCoin(coin, userId);
    } catch (e) {
      throw e.toString();
    }
  }

  async getOpenPositionForUser(
    userId: number,
    count: number,
    pageNumber: number
  ) {
    try {
      const data = await this._positionRepository.getOpenPositionForUser(
        userId
      );
      return getPositionDataForOpen(data, count, pageNumber);
    } catch (e) {
      throw e.toString();
    }
  }

  async getHistoryPositionForUser(
    userId: number,
    count: number,
    pageNumber: number
  ) {
    try {
      const data = await this._positionRepository.getHistoryPositionForUser(
        userId
      );
      return getPositionDataForHistory(data, count, pageNumber);
    } catch (e) {
      throw e.toString();
    }
  }
}
