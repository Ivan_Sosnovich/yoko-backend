import { Controller, UseGuards, Get, Req, Res, Param } from "@nestjs/common";
import { ApiTags, ApiHeader, ApiResponse, ApiParam } from "@nestjs/swagger";
import { Request, Response } from "express";
import { ResponseError } from "../../shared/types/server/response_error";
import { HttpStatus } from "../../shared/types/server/http_status";
import { JwtAuthGuard } from "../../guards/JwtGuard";
import { IUser } from "../user/user.types";
import { PositionsService } from "./positions.service";
import {
  GetPositionForHistoryReponse,
  GetPositionForOpenReponse,
  GetPositionForYear,
} from "./dto";

@ApiTags("position controller")
@Controller("position")
export class PositionsController {
  constructor(private _positionService: PositionsService) {}

  // получения данных для графика
  @UseGuards(JwtAuthGuard)
  @Get("/year")
  @ApiHeader({
    name: "Authorization",
    required: true,
    description: "Bearer token",
  })
  @ApiResponse({ status: 200, type: GetPositionForYear })
  @ApiResponse({
    status: 404,
    type: ResponseError,
  })
  getPositionForYear(@Req() request: Request, @Res() response: Response) {
    const { id } = request.user as IUser;
    this._positionService
      .getPositionForYear(id)
      .then((data) => response.status(200).send(data))
      .catch((e) =>
        response
          .status(HttpStatus.BAD_REQUEST)
          .send({ status: HttpStatus.BAD_REQUEST, message: e.toString() })
      );
  }

  // получения данных по открытым сделкам
  @UseGuards(JwtAuthGuard)
  @Get("/open/:count/:pageNumber")
  @ApiHeader({
    name: "Authorization",
    required: true,
    description: "Bearer token",
  })
  @ApiParam({
    name: "count",
    description: "Количество строк для отображения",
    schema: { type: "number" },
  })
  @ApiParam({
    name: "pageNumber",
    description: "Номер страницы для отображения",
    schema: { type: "number" },
  })
  @ApiResponse({ status: 200, type: GetPositionForOpenReponse })
  @ApiResponse({
    status: 404,
    type: ResponseError,
  })
  getOpenPositionForUser(
    @Req() request: Request,
    @Res() response: Response,
    @Param() { count, pageNumber }
  ) {
    const { id } = request.user as IUser;
    this._positionService
      .getOpenPositionForUser(id, count, pageNumber)
      .then((data) => response.status(200).send(data))
      .catch((e) =>
        response
          .status(HttpStatus.BAD_REQUEST)
          .send({ status: HttpStatus.BAD_REQUEST, message: e.toString() })
      );
  }
  // получения данных по закрытым сделкам
  @UseGuards(JwtAuthGuard)
  @Get("/history/:count/:pageNumber")
  @ApiHeader({
    name: "Authorization",
    required: true,
    description: "Bearer token",
  })
  @ApiParam({
    name: "count",
    description: "Количество строк для отображения",
    schema: { type: "number" },
  })
  @ApiParam({
    name: "pageNumber",
    description: "Номер страницы для отображения",
    schema: { type: "number" },
  })
  @ApiResponse({ status: 200, type: GetPositionForHistoryReponse })
  @ApiResponse({
    status: 404,
    type: ResponseError,
  })
  getHistoryPositionForUser(
    @Req() request: Request,
    @Res() response: Response,
    @Param() { count, pageNumber }
  ) {
    const { id } = request.user as IUser;
    this._positionService
      .getHistoryPositionForUser(id, count, pageNumber)
      .then((data) => response.status(200).send(data))
      .catch((e) =>
        response
          .status(HttpStatus.BAD_REQUEST)
          .send({ status: HttpStatus.BAD_REQUEST, message: e.toString() })
      );
  }
}
