import { Module } from "@nestjs/common";
import { DatabaseModule } from "../../database/database.module";
import { positionProviders } from "./positions.providers";
import { PositionsService } from "./positions.service";
import { PositionsController } from "./positions.controller";
import { PositionsRepository } from "./positions.repository";

@Module({
  imports: [DatabaseModule],
  providers: [...positionProviders, PositionsService, PositionsRepository],
  controllers: [PositionsController],
  exports: [PositionsService, PositionsRepository],
})
export class PositionsModule {}
