import { IPosition } from "../position.types";

export class GetPositionForHistoryReponse {
  profit: number;
  count: number;
  data: HistoryPositionData[];
}

export class HistoryPositionData {
  position: Pick<
    IPosition,
    "symbol" | "size" | "targetPrice" | "profit" | "commissions"
  > & { period: string };
  positions: Pick<
    IPosition,
    "openingTime" | "size" | "openingPrice" | "profit"
  >[];
}
