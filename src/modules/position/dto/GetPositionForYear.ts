import { ApiProperty } from "@nestjs/swagger";

export class GetPositionForYear {
  @ApiProperty()
  data: number[];
}
