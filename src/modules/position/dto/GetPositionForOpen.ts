import { IPosition } from "../position.types";

export class GetPositionForOpenReponse {
  count: number;
  data: OpenPositionData[];
  nested: number;
}

export class OpenPositionData {
  position: Pick<IPosition, "openingTime" | "symbol" | "size" | "targetPrice">;
  positions: Pick<IPosition, "openingTime" | "size" | "openingPrice">[];
}
