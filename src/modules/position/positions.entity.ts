import {
  Column,
  Entity,
  CreateDateColumn,
  UpdateDateColumn,
  PrimaryGeneratedColumn,
} from "typeorm";
import { IPosition } from "./position.types";
import { ColumnNumericTransformer } from "../../shared/lib/ColumnNumericTransformer";
import { POSITION_TABLE_NAME } from "../../shared/constants/tableName";
import { PositionDirection } from "../../shared/constants/positionDirection";
@Entity(POSITION_TABLE_NAME)
export class PositionsEntity implements IPosition {
  @PrimaryGeneratedColumn("increment")
  id: number;

  @Column({ type: "text" })
  symbol: string;

  @Column({ type: "boolean" })
  isOpen: boolean;

  @Column({
    type: "enum",
    enum: [
      PositionDirection.USER,
      PositionDirection.SELL,
      PositionDirection.BUY,
    ],
  })
  direction: PositionDirection;

  @Column({ type: "numeric", transformer: new ColumnNumericTransformer() })
  size: number;

  @Column({ type: "numeric", transformer: new ColumnNumericTransformer() })
  openingPrice: number;

  @Column({
    type: "numeric",
    nullable: true,
    transformer: new ColumnNumericTransformer(),
  })
  closingPrice: number;

  @CreateDateColumn({ name: "openingTime" })
  openingTime: Date;

  @UpdateDateColumn({ name: "closingTime", nullable: true })
  closingTime: Date;

  @Column({
    type: "numeric",
    nullable: true,
    transformer: new ColumnNumericTransformer(),
  })
  commissions: number;

  @Column({ type: "numeric", transformer: new ColumnNumericTransformer() })
  targetPrice: number;

  @Column({ type: "numeric", nullable: true })
  parentPositionId: number;

  @Column({
    type: "numeric",
    nullable: true,
    transformer: new ColumnNumericTransformer(),
  })
  profit: number;

  @Column({ type: "numeric" })
  userId: number;

  @Column({ type: "boolean" })
  isParentPositions: boolean;
}
