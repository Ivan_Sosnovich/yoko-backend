import { AppDataSource } from "../../database/data-source";
import { DATABASE_CONNECTION } from "../../database/database.providers";
import { PositionsEntity } from "./positions.entity";
import { POSITION_TABLE_NAME } from "../../shared/constants/tableName";

export const positionProviders = [
  {
    provide: POSITION_TABLE_NAME,
    useFactory: () => AppDataSource.getRepository(PositionsEntity),
    inject: [DATABASE_CONNECTION],
  },
];
