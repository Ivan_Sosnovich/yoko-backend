import { GetPositionForOpenReponse, OpenPositionData } from "../../dto";
import { PositionsEntity } from "../../positions.entity";

export const getPositionDataForOpen = (
  data: PositionsEntity[],
  count: number,
  pageNumber: number
): GetPositionForOpenReponse => {
  // формируем список позиций и главной позиции
  const positionData = new Map<string, OpenPositionData>();
  const res: OpenPositionData[] = [];
  let nested = 0;

  data.forEach((position) => {
    const { symbol, openingTime, size, targetPrice, openingPrice } = position;
    nested += openingPrice * size;

    if (positionData.has(symbol)) {
      const { position, positions } = positionData.get(symbol);

      positionData.set(symbol, {
        position: {
          symbol: position.symbol.replace("_", " -> "),
          openingTime: position.openingTime,
          size: position.size + size,
          targetPrice:
            targetPrice > position.targetPrice
              ? targetPrice
              : position.targetPrice,
        },
        positions: [...positions, { openingTime, size, openingPrice }],
      });
    }
    if (!positionData.has(symbol)) {
      positionData.set(symbol, {
        position: {
          symbol: symbol.replace("_", " -> "),
          openingTime,
          size,
          targetPrice,
        },
        positions: [{ openingTime, size, openingPrice }],
      });
    }
  });

  positionData.forEach((value) => res.push(value));

  return {
    nested,
    count: Math.ceil(res.length / count),
    data: res.splice(pageNumber * count, count + pageNumber * count),
  };
};
