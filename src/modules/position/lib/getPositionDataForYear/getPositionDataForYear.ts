import { PositionsEntity } from "../../positions.entity";

export const getPositionDataForYear = (userPosition: PositionsEntity[]) => {
  const chartData = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
  const monthsData = new Map<number, number>();

  userPosition.forEach(({ profit, closingTime }) => {
    const month = closingTime.getMonth();

    if (monthsData.has(month)) {
      const oldValue = monthsData.get(month);
      monthsData.set(month, oldValue + profit);
    }
    if (!monthsData.has(month)) {
      monthsData.set(month, profit);
    }
  });

  monthsData.forEach((value, key) => (chartData[key] = value));
  return chartData;
};
