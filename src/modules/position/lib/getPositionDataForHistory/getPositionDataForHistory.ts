import { GetPositionForHistoryReponse, HistoryPositionData } from "../../dto";
import { PositionsEntity } from "../../positions.entity";

export const getPositionDataForHistory = (
  data: PositionsEntity[],
  count: number,
  pageNumber: number
): GetPositionForHistoryReponse => {
  // формируем все позиции по главной

  const mappedPositions = data.reduce(
    (acc: Record<number, PositionsEntity[]>, value) => {
      const { id, parentPositionId, isParentPositions } = value;

      if (acc[parentPositionId] && !isParentPositions) {
        acc[parentPositionId].push(value);
      }

      if (!acc[parentPositionId] && !isParentPositions) {
        acc[parentPositionId] = [value];
      }

      if (isParentPositions && acc[id]) {
        acc[id].push(value);
      }

      if (isParentPositions && !acc[id]) {
        acc[id] = [value];
      }
      return acc;
    },
    {}
  );

  let profit = 0;
  const historyPositionData: HistoryPositionData[] = [];

  for (const [_, value] of Object.entries(mappedPositions)) {
    const historyPosition: HistoryPositionData = value.reduce(
      (acc: HistoryPositionData, position) => {
        const {
          symbol,
          size,
          targetPrice,--allow-unrelated-histories
          profit,
          openingTime,
          closingTime,
          openingPrice,
        } = position;
        return {
          position: {
            symbol: symbol.replace("_", " -> "),
            size: acc.position.size + size,
            targetPrice:
              acc.position.targetPrice > targetPrice
                ? acc.position.targetPrice
                : targetPrice,
            profit: acc.position.profit + profit,
            commissions: (acc.position.profit + profit) * 0.2,
            period: `${openingTime}-${closingTime}`,
          },
          positions: [
            ...acc.positions,
            { openingTime, size, openingPrice, profit },
          ],
        };
      },
      {
        position: {
          symbol: "",
          size: 0,
          targetPrice: 0,
          profit: 0,
          commissions: 0,
          period: "",
        },
        positions: [],
      }
    );
    profit += historyPosition.position.profit;
    historyPositionData.push(historyPosition);
  }
  return {
    profit,
    count: Math.ceil(historyPositionData.length / count),
    data: historyPositionData.splice(
      pageNumber * count,
      count + pageNumber * count
    ),
  };
};
