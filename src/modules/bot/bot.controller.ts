import {
  Body,
  Controller,
  Get,
  Post,
  Req,
  Res,
  UseGuards,
} from "@nestjs/common";
import { ApiTags, ApiResponse, ApiHeader, ApiBody } from "@nestjs/swagger";
import { Response, Request } from "express";
import { BotService } from "./bot.service";
import { JwtAuthGuard } from "../../guards/JwtGuard";
import {
  ResponseError,
  ResponseSuccess,
} from "../../shared/types/server/response_error";
import {
  GetBotDataResponceDTO,
  GetBotSettingsResponceDTO,
} from "./botSettings/dto";
import { IUser } from "../user/user.types";
import { HttpStatus } from "../../shared/types/server/http_status";
import { ChangeBotStatusDTO } from "./dto";
import { ChangeBotSettingsDto } from "./botSettings/dto/ChangeBotSettingsDto";

@ApiTags("bot controller")
@Controller("bot")
export class BotController {
  constructor(private _botService: BotService) {}

  @UseGuards(JwtAuthGuard)
  @Get("")
  @ApiHeader({
    name: "Authorization",
    required: true,
    description: "Bearer token",
  })
  @ApiResponse({ status: 200, type: GetBotDataResponceDTO })
  @ApiResponse({
    status: 401,
    description: "Ошибка авторизации",
    type: ResponseError,
  })
  @ApiResponse({
    status: 404,
    description: "Ошибки получения модели бота",
    type: ResponseError,
  })
  getBot(@Req() request: Request, @Res() response: Response) {
    const user = request.user as IUser;
    this._botService
      .getBotDataForUser(user)
      .then((data) => response.status(200).send(data))
      .catch((e) =>
        response.status(HttpStatus.BAD_REQUEST).send({
          status: HttpStatus.NOT_FOUND,
          message: e.toString(),
        })
      );
  }

  @UseGuards(JwtAuthGuard)
  @Get("/settings")
  @ApiHeader({
    name: "Authorization",
    required: true,
    description: "Bearer token",
  })
  @ApiResponse({ status: 200, type: GetBotSettingsResponceDTO })
  @ApiResponse({
    status: 401,
    description: "Ошибка авторизации",
    type: ResponseError,
  })
  @ApiResponse({
    status: 404,
    description: "Ошибки получения настроек бота",
    type: ResponseError,
  })
  getBotSettings(@Req() request: Request, @Res() response: Response) {
    const user = request.user as IUser;
    this._botService
      .getBotSettingsForUser(user)
      .then((data) => response.status(200).send(data))
      .catch((e) =>
        response.status(HttpStatus.BAD_REQUEST).send({
          status: HttpStatus.NOT_FOUND,
          message: e.toString(),
        })
      );
  }

  @UseGuards(JwtAuthGuard)
  @Post("/settings")
  @ApiHeader({
    name: "Authorization",
    required: true,
    description: "Bearer token",
  })
  @ApiBody({ type: ChangeBotStatusDTO })
  @ApiResponse({ status: 200, type: ResponseSuccess })
  @ApiResponse({
    status: 401,
    description: "Ошибка авторизации",
    type: ResponseError,
  })
  @ApiResponse({
    status: 404,
    description: "Ошибки изменения настроек бота",
    type: ResponseError,
  })
  changeBotSettings(
    @Body() botSettingsDto: ChangeBotSettingsDto,
    @Req() request: Request,
    @Res() response: Response
  ) {
    const user = request.user as IUser;
    this._botService
      .changeBotSettings(user, botSettingsDto)
      .then(() => response.status(200).send({ message: "success" }))
      .catch((e) =>
        response.status(HttpStatus.BAD_REQUEST).send({
          status: HttpStatus.NOT_FOUND,
          message: e.toString(),
        })
      );
  }

  @UseGuards(JwtAuthGuard)
  @Post("/settings/status")
  @ApiHeader({
    name: "Authorization",
    required: true,
    description: "Bearer token",
  })
  @ApiBody({ type: ChangeBotStatusDTO })
  @ApiResponse({ status: 200, type: ResponseSuccess })
  @ApiResponse({
    status: 401,
    description: "Ошибка авторизации",
    type: ResponseError,
  })
  @ApiResponse({
    status: 404,
    description: "Ошибки изменения настроек бота",
    type: ResponseError,
  })
  changeBotStatusForUser(
    @Body() changeBotStatusDTO: ChangeBotStatusDTO,
    @Req() request: Request,
    @Res() response: Response
  ) {
    const user = request.user as IUser;
    this._botService
      .changeBotStatusForUserID(user, changeBotStatusDTO)
      .then(() => response.status(200).send({ message: "success" }))
      .catch((e) =>
        response.status(HttpStatus.BAD_REQUEST).send({
          status: HttpStatus.NOT_FOUND,
          message: e.toString(),
        })
      );
  }
}
