import { forwardRef, Module } from "@nestjs/common";
import * as https from "https";
import { HttpModule } from "@nestjs/axios";
import { DatabaseModule } from "src/database/database.module";
import { BotController } from "./bot.controller";
import { botProviders } from "./bot.providers";
import { botSettingsProviders } from "./botSettings/botSettings.providers";
import { BotRepository } from "./bot.repository";
import { BotService } from "./bot.service";
import { BotGateway } from "./bot.gateway";
import { TradingMachineModule } from "../tradingMachine/tradingMachine.module";

const httpsAgent = new https.Agent({ rejectUnauthorized: false });

@Module({
  imports: [
    DatabaseModule,
    forwardRef(() => TradingMachineModule),
    HttpModule.register({
      httpsAgent,
    }),
  ],
  providers: [
    ...botProviders,
    ...botSettingsProviders,
    BotRepository,
    BotService,
    BotGateway,
  ],
  controllers: [BotController],
  exports: [BotService, BotRepository],
})
export class BotModule {}
