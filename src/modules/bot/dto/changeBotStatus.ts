import { ApiProperty } from "@nestjs/swagger";
import { IsEnum, IsNotEmpty } from "class-validator";
import { BotStatus } from "../../../shared/constants/botStatus";

export class ChangeBotStatusDTO {
  @ApiProperty({ required: true })
  @IsNotEmpty()
  @IsEnum(BotStatus)
  status: BotStatus;
}
