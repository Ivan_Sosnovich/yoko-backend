import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";
import { ColumnNumericTransformer } from "../../shared/lib/ColumnNumericTransformer";
import { BOT_TABLE_NAME } from "../../shared/constants/tableName";
import { IBot } from "./bot.types";
import { BotStatus } from "../../shared/constants/botStatus";

@Entity(BOT_TABLE_NAME)
export class BotEntity implements IBot {
  @PrimaryGeneratedColumn("increment")
  id: number;

  @Column({ type: "text" })
  login: string;

  @Column({ type: "text" })
  password: string;

  @Column({
    type: "enum",
    enum: [BotStatus.CREATE, BotStatus.OFF, BotStatus.ON, BotStatus.ERROR],
  })
  status: BotStatus;

  @Column({
    type: "numeric",
    nullable: true,
    transformer: new ColumnNumericTransformer(),
  })
  userId: number;

  @Column({
    type: "numeric",
    nullable: true,
    transformer: new ColumnNumericTransformer(),
  })
  error: number;

  @Column({ type: "boolean", nullable: false })
  isCreated: boolean;
}
