import { HttpService } from "@nestjs/axios";
import { catchError, firstValueFrom } from "rxjs";
import { Injectable, Logger } from "@nestjs/common";
import { LoginBotDto } from "../tradingMachine/dto";
import { BotRepository } from "./bot.repository";
import { BotStatus } from "../../shared/constants/botStatus";
import { IUser } from "../user/user.types";
import { keyEncryptionByCrypto } from "../../shared/lib/keyEncryptionByCrypto";
import { BotGateway } from "./bot.gateway";
import { ChangeBotStatusDTO } from "./dto";
import { createBotData } from "./lib/createBotData";
import { createBotSettings } from "./lib/createBotSettings";
import { AxiosError } from "axios";
import { ChangeBotSettingsDto } from "./botSettings/dto";

@Injectable()
export class BotService {
  private readonly logger = new Logger(BotService.name);
  constructor(
    private _botRepository: BotRepository,
    private _botGetaway: BotGateway,
    private _httpService: HttpService
  ) {}

  async botLogin({ login, password }: LoginBotDto) {
    this.logger.warn(`Поиск бота в базе по логину: ${login}`);
    try {
      const bot = await this._botRepository.searchBotForLogin(login);
      if (!bot) {
        throw Error(`Бот по логину: ${login}, не найден`);
      }
      this.logger.log(`В базе найден бот по id: ${bot.id}, сверяем пароли`);
      const validatePassword = bot.password === password;
      if (validatePassword) {
        return bot;
      }
      throw Error("Пароли не совпадают");
    } catch (e) {
      this.logger.error(`При поиски бота в базе, произошла ошибка: ${e}`);
      throw e;
    }
  }

  async changeBotStatus(id: number, status: BotStatus, error?: number) {
    this.logger.log(
      `Изменение статуса бота по id: ${id}, на статус: ${status}${
        status === BotStatus.ERROR ? ` с ошибкой ${error}` : ""
      }`
    );
    try {
      await this._botRepository.changeBotStatus(id, status, error);
    } catch (e) {
      throw e;
    }
  }

  async getBotSettings(id: number) {
    try {
      return this._botRepository.getBotSettings(id);
    } catch (e) {
      throw e.toString();
    }
  }

  async getBotDataForUser(user: IUser) {
    try {
      const { id } = user;
      this.logger.warn(
        `Поступил запрос от пользователя по id: ${id}, на модель данных бота`
      );
      const bot = await this._botRepository.getBotDataForUserId(id);
      if (!bot) {
        throw Error(`Для пользователя по id: ${id}, бот не найден`);
      }
      const { id: botId, status, error } = bot;
      return { id: botId, status, error };
    } catch (e) {
      this.logger.error(
        `При получении модели данных бота для пользователя по id: ${
          user.id
        }, произошла ошибка: ${e.toString()}`
      );
      throw e.toString();
    }
  }

  async getBotSettingsForUser(user: IUser) {
    try {
      const { id } = user;
      this.logger.warn(
        `Поступил запрос от пользователя по id: ${id}, на получение настроек бота`
      );
      const bot = await this._botRepository.getBotDataForUserId(id);
      if (!bot) {
        throw Error(`Для пользователя по id: ${id}, бот не найден`);
      }
      const { id: botId } = bot;
      const botSettings = await this._botRepository.getBotSettings(botId);
      if (!botSettings) {
        throw Error(`Для пользователя по id: ${id}, настройки бота не найдены`);
      }
      return botSettings;
    } catch (e) {
      this.logger.error(
        `При получении настроек бота для пользователя по id: ${
          user.id
        }, произошла ошибка: ${e.toString()}`
      );
      throw e.toString();
    }
  }

  async changeBotSettings(user: IUser, botSettingsDto: ChangeBotSettingsDto) {
    try {
      const { id } = user;
      this.logger.warn(
        `От пользователя по id:${id}, поступил запрос на изменение настроек бота`
      );
      if (!Object.keys(botSettingsDto).length) {
        throw Error(`От пользователя по id: ${id}, не пришли настройки бота`);
      }
      const userBot = await this._botRepository.searchBotForUserId(id);
      if (!userBot) {
        throw Error(`Для пользователя по id: ${id}, бот не найден`);
      }
      this.logger.log(
        `Для пользователя по id: ${id}, найден бот по id: ${userBot.id}`
      );
      await this._botRepository.changeBotSettings(userBot.id, botSettingsDto);
      const newBotSettings: ChangeBotSettingsDto = {
        ...botSettingsDto,
        ...(botSettingsDto?.apiKey
          ? { apiKey: keyEncryptionByCrypto(botSettingsDto.apiKey) }
          : {}),
        ...(botSettingsDto?.secretKey
          ? { secretKey: keyEncryptionByCrypto(botSettingsDto.secretKey) }
          : {}),
      };
      this.logger.warn(
        `Отправка события изменения настроек бота по id: ${userBot.id}`
      );
      await this._botGetaway.sendBotSettings$(
        newBotSettings,
        userBot.id,
        userBot.status
      );
    } catch (e) {
      this.logger.error(
        `При изменении настроек бота для пользователя по id: ${
          user.id
        }, произошла ошибка: ${e.toString()}`
      );
      throw e.toString();
    }
  }

  async getBotDataForID(id: number) {
    try {
      return this._botRepository.getBotDataForID(id);
    } catch (e) {
      throw e.toString();
    }
  }

  async changeBotStatusForUserID(
    user: IUser,
    changeBotStatusDTO: ChangeBotStatusDTO
  ) {
    try {
      const { id, login, password, isCreated } =
        await this._botRepository.searchBotForUserId(user.id);
      const { apiAdres } = await this._botRepository.getBotSettings(id);
      const { status } = changeBotStatusDTO;
      if (!id) {
        throw Error(`Бот для пользователя по id: ${user.id}, не найден`);
      }
      if (status === BotStatus.ON && !isCreated) {
        this.logger.warn(
          `Пользователя под id: ${user.id}, первый раз активировал бота, передаем на ботов, событие создания бота`
        );
        await this._botRepository.changeBotStatus(id, BotStatus.CREATE);

        await this._botRepository.changeBotStatusCreated(id, true);
        await this._createBotInCluster(user.id, login, password, apiAdres);
      } else {
        await this._botGetaway.sendBotChangeStatus$(id, status);
        await this._botRepository.changeBotStatus(id, status);
      }
    } catch (e) {
      this.logger.error(
        `При изменении статуса бота возникла ошибка: ${
          e ? e.toString() : "Ошибка отправки запроса на создание бота"
        }`
      );
      throw e ? e.toString() : "Ошибка отправки запроса на создание бота";
    }
  }

  async createBot(userId: number) {
    try {
      const newBotData = createBotData(userId);
      const { id } = await this._botRepository.createBotDataForUser(
        userId,
        newBotData
      );
      const newBotSettings = createBotSettings(id);
      await this._botRepository.createBotSettingsForBot(id, newBotSettings);
    } catch (e) {
      throw e.toString();
    }
  }

  _createBotInCluster = async (
    id: number,
    login: string,
    password: string,
    apiAdres: string
  ) => {
    const url = process.env.BOT_SERVER_API;
    const BOT_SECRET_KEY = process.env.BOT_SECRET_KEY;

    await firstValueFrom(
      this._httpService
        .request({
          method: "POST",
          url,
          headers: {
            "Content-type": "application/json",
            Authorization:
              "Bearer dop_v1_93ff9e6bc1873b811ff19f79d92ae4276ace81cdacb9d8afecd8430e3c192602",
          },
          data: {
            apiVersion: "v1",
            kind: "Pod",
            metadata: {
              name: `tradeuser${id}`,
            },
            spec: {
              containers: [
                {
                  name: "bot",
                  image:
                    "registry.digitalocean.com/yoko-registry/tradebot:latest",
                  imagePullPolicy: "Always",
                  ports: [
                    {
                      containerPort: 80,
                    },
                  ],
                  env: [
                    {
                      name: "BOT_LOGIN",
                      value: login,
                    },
                    {
                      name: "BOT_PASSWORD",
                      value: password,
                    },
                    {
                      name: "BOT_SECRET_KEY",
                      value: BOT_SECRET_KEY,
                    },
                    {
                      name: "HTTPS_PROXY",
                      value: apiAdres,
                    },
                  ],
                },
              ],
            },
          },
        })
        .pipe(
          catchError((error: AxiosError) => {
            this.logger.error(error);
            throw error;
          })
        )
    );
  };
}
