import { DATABASE_CONNECTION } from "../../database/database.providers";
import { AppDataSource } from "../../database/data-source";
import { BotEntity } from "./bot.entity";
import { BOT_TABLE_NAME } from "../../shared/constants/tableName";

export const botProviders = [
  {
    provide: BOT_TABLE_NAME,
    useFactory: () => AppDataSource.getRepository(BotEntity),
    inject: [DATABASE_CONNECTION],
  },
];
