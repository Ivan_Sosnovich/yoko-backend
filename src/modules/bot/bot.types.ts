import { BotStatus } from "../../shared/constants/botStatus";

export type IBot = {
  id?: number;
  login: string;
  password: string;
  isCreated: boolean;
  status: BotStatus;
  userId: number;
  error?: number;
};

export interface IBotJwt {
  id?: number;
  userId: number;
  login: string;
  password: string;
}
