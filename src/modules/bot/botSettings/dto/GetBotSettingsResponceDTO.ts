import { ApiProperty } from "@nestjs/swagger";

export class GetBotSettingsResponceDTO {
  @ApiProperty()
  id: number;
  @ApiProperty()
  minBnb: number;
  @ApiProperty()
  minBalance: number;
  @ApiProperty()
  maxTraidePairs: number;
  @ApiProperty()
  minOrder: number;
  @ApiProperty()
  orderTimer: number;
  @ApiProperty()
  minValue: number;
  @ApiProperty()
  sellUp: number;
  @ApiProperty()
  buyDown: number;
  @ApiProperty()
  numAver: boolean;
  @ApiProperty()
  stepAver: number;
  @ApiProperty()
  maxAver: number;
  @ApiProperty()
  quantityAver: number;
  @ApiProperty()
  apiKey: string;
  @ApiProperty()
  secretKey: string;
  @ApiProperty()
  minDailyPercent: number;
  @ApiProperty()
  dailyPercent: number;
  @ApiProperty()
  progressiveMaxPairs: boolean;
  @ApiProperty()
  progressiveAverage: boolean;
  @ApiProperty()
  averagePercent: number;
  @ApiProperty()
  trailingStop: boolean;
  @ApiProperty()
  trailingPercent: number;
  @ApiProperty({ required: false })
  status: string;
  @ApiProperty()
  blockedPairs: string[];
  @ApiProperty()
  allowedPairs: string[];
}
