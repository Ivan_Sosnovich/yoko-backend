import { ApiProperty } from "@nestjs/swagger";
import {
  IsString,
  IsNumber,
  IsBoolean,
  IsArray,
  IsOptional,
} from "class-validator";

export class ChangeBotSettingsDto {
  @ApiProperty()
  @IsNumber()
  @IsOptional()
  minBnb: number;
  @ApiProperty()
  @IsNumber()
  @IsOptional()
  minBalance: number;
  @ApiProperty()
  @IsNumber()
  @IsOptional()
  maxTraidePairs: number;
  @ApiProperty()
  @IsNumber()
  @IsOptional()
  minOrder: number;
  @ApiProperty()
  @IsNumber()
  @IsOptional()
  orderTimer: number;
  @ApiProperty()
  @IsNumber()
  @IsOptional()
  minValue: number;
  @ApiProperty()
  @IsNumber()
  @IsOptional()
  sellUp: number;
  @ApiProperty()
  @IsNumber()
  @IsOptional()
  buyDown: number;
  @ApiProperty()
  @IsBoolean()
  @IsOptional()
  numAver: boolean;
  @ApiProperty()
  @IsNumber()
  @IsOptional()
  stepAver: number;
  @ApiProperty()
  @IsNumber()
  @IsOptional()
  maxAver: number;
  @ApiProperty()
  @IsNumber()
  @IsOptional()
  quantityAver: number;
  @ApiProperty()
  @IsString()
  @IsOptional()
  apiKey: string;
  @ApiProperty()
  @IsString()
  @IsOptional()
  secretKey: string;
  @ApiProperty()
  @IsNumber()
  @IsOptional()
  minDailyPercent: number;
  @ApiProperty()
  @IsNumber()
  @IsOptional()
  dailyPercent: number;
  @ApiProperty()
  @IsBoolean()
  @IsOptional()
  progressiveMaxPairs: boolean;
  @ApiProperty()
  @IsBoolean()
  @IsOptional()
  progressiveAverage: boolean;
  @ApiProperty()
  @IsNumber()
  @IsOptional()
  averagePercent: number;
  @ApiProperty()
  @IsBoolean()
  @IsOptional()
  trailingStop: boolean;
  @ApiProperty()
  @IsNumber()
  @IsOptional()
  trailingPercent: number;
  @ApiProperty({ required: false })
  @ApiProperty()
  @IsArray()
  @IsOptional()
  blockedPairs: string[];
  @ApiProperty()
  @IsArray()
  @IsOptional()
  allowedPairs: string[];
}
