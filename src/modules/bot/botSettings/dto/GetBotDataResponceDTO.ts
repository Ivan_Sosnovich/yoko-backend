import { ApiProperty } from "@nestjs/swagger";
import { BotStatus } from "../../../../shared/constants/botStatus";

export class GetBotDataResponceDTO {
  @ApiProperty()
  id: number;
  @ApiProperty()
  status: BotStatus;
  @ApiProperty()
  error: number;
}
