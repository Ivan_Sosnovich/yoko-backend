import { DATABASE_CONNECTION } from "../../../database/database.providers";
import { AppDataSource } from "../../../database/data-source";
import { BotSettingsEntity } from "./botSettings.entity";
import { BOT_SETTINGS_TABLE_NAME } from "../../../shared/constants/tableName";

export const botSettingsProviders = [
  {
    provide: BOT_SETTINGS_TABLE_NAME,
    useFactory: () => AppDataSource.getRepository(BotSettingsEntity),
    inject: [DATABASE_CONNECTION],
  },
];
