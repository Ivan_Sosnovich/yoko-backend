export interface IBotSettings {
  id?: number;
  minBnb: number;
  minBalance: number;
  maxTraidePairs: number;
  minOrder: number;
  orderTimer: number;
  minValue: number;
  sellUp: number;
  buyDown: number;
  numAver: boolean;
  stepAver: number;
  maxAver: number;
  quantityAver: number;
  apiKey: string;
  secretKey: string;
  minDailyPercent: number;
  dailyPercent: number;
  progressiveMaxPairs: boolean;
  progressiveAverage: boolean;
  averagePercent: number;
  trailingStop: boolean;
  trailingPercent: number;
  botId: number;
  blockedPairs: string[];
  allowedPairs: string[];
  apiAdres: string;
}
