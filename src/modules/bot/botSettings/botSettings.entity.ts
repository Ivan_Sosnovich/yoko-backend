import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";
import { ColumnNumericTransformer } from "../../../shared/lib/ColumnNumericTransformer";
import { BOT_SETTINGS_TABLE_NAME } from "../../../shared/constants/tableName";
import { IBotSettings } from "./botSettings.types";

@Entity(BOT_SETTINGS_TABLE_NAME)
export class BotSettingsEntity implements IBotSettings {
  @PrimaryGeneratedColumn("increment")
  id: number;
  @Column({ type: "numeric", transformer: new ColumnNumericTransformer() })
  minBnb: number;
  @Column({ type: "numeric", transformer: new ColumnNumericTransformer() })
  minBalance: number;
  @Column({ type: "numeric", transformer: new ColumnNumericTransformer() })
  maxTraidePairs: number;
  @Column({ type: "numeric", transformer: new ColumnNumericTransformer() })
  minOrder: number;
  @Column({ type: "numeric", transformer: new ColumnNumericTransformer() })
  orderTimer: number;
  @Column({ type: "numeric", transformer: new ColumnNumericTransformer() })
  botId: number;
  @Column({ type: "numeric", transformer: new ColumnNumericTransformer() })
  minValue: number;
  @Column({ type: "numeric", transformer: new ColumnNumericTransformer() })
  sellUp: number;
  @Column({ type: "numeric", transformer: new ColumnNumericTransformer() })
  buyDown: number;
  @Column({ type: "boolean" })
  numAver: boolean;
  @Column({ type: "numeric", transformer: new ColumnNumericTransformer() })
  stepAver: number;
  @Column({ type: "numeric", transformer: new ColumnNumericTransformer() })
  maxAver: number;
  @Column({ type: "numeric", transformer: new ColumnNumericTransformer() })
  quantityAver: number;
  @Column({ type: "text" })
  apiAdres: string;
  @Column({ type: "text" })
  apiKey: string;
  @Column({ type: "text" })
  secretKey: string;
  @Column({ type: "numeric", transformer: new ColumnNumericTransformer() })
  minDailyPercent: number;
  @Column({ type: "numeric", transformer: new ColumnNumericTransformer() })
  dailyPercent: number;
  @Column({ type: "boolean" })
  progressiveMaxPairs: boolean;
  @Column({ type: "boolean" })
  progressiveAverage: boolean;
  @Column({ type: "numeric", transformer: new ColumnNumericTransformer() })
  averagePercent: number;
  @Column({ type: "boolean" })
  trailingStop: boolean;
  @Column({ type: "numeric", transformer: new ColumnNumericTransformer() })
  trailingPercent: number;
  @Column("text", { array: true })
  blockedPairs: string[];
  @Column("text", { array: true })
  allowedPairs: string[];
}
