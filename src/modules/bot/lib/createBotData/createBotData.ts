import { IBot } from "../../bot.types";
import { BotStatus } from "../../../../shared/constants/botStatus";
import { keyEncryptionByCrypto } from "../../../../shared/lib/keyEncryptionByCrypto";

export const createBotData = (userId: number): IBot => {
  const randomNumber = Math.random() * 1000 + Math.random() * 1000;
  const hashValueForLogin = `${userId * randomNumber * userId}${
    Math.random() * 1000
  }`;
  const hashValueForPassword = `${randomNumber * userId * randomNumber}${
    Math.random() * 1000
  }`;
  const login = keyEncryptionByCrypto(hashValueForLogin);
  const password = keyEncryptionByCrypto(hashValueForPassword);
  return {
    isCreated: false,
    login,
    password,
    status: BotStatus.OFF,
    userId,
  };
};
