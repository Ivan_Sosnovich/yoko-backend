import { Injectable, Inject } from "@nestjs/common";
import { Repository } from "typeorm";
import {
  BOT_SETTINGS_TABLE_NAME,
  BOT_TABLE_NAME,
} from "../../shared/constants/tableName";
import { BotEntity } from "./bot.entity";
import { BotStatus } from "../../shared/constants/botStatus";
import { BotSettingsEntity } from "./botSettings/botSettings.entity";
import { IBot } from "./bot.types";
import { IBotSettings } from "./botSettings/botSettings.types";
import { ChangeBotSettingsDto } from "./botSettings/dto/ChangeBotSettingsDto";
@Injectable()
export class BotRepository {
  constructor(
    @Inject(BOT_TABLE_NAME)
    private _botRepository: Repository<BotEntity>,
    @Inject(BOT_SETTINGS_TABLE_NAME)
    private _botSettingsRepository: Repository<BotSettingsEntity>
  ) {}

  public searchBotForLogin(login: string) {
    return this._botRepository.findOne({
      where: {
        login,
      },
    });
  }

  public async changeBotStatus(id: number, status: BotStatus, error?: number) {
    await this._botRepository.update(id, {
      status,
      error: error || null,
    });
  }

  public async changeBotStatusCreated(id: number, status: boolean) {
    await this._botRepository.update(id, { isCreated: status });
  }

  public getBotSettings(id: number) {
    return this._botSettingsRepository.findOneBy({ botId: id });
  }

  public getBotDataForUserId(userId: number) {
    return this._botRepository.findOneBy({ userId });
  }

  public async changeBotSettings(
    botId: number,
    botSettingsDto: ChangeBotSettingsDto
  ) {
    const botBase = await this._botSettingsRepository.findOneBy({ botId });
    return this._botSettingsRepository.save({
      botId,
      ...botBase,
      ...botSettingsDto,
    });
  }

  public searchBotForUserId(userId: number) {
    return this._botRepository.findOneBy({ userId });
  }

  public getBotDataForID(id: number) {
    return this._botRepository.findOneBy({ id });
  }

  public createBotDataForUser(id: number, newBot: IBot) {
    return this._botRepository.save({
      ...new BotEntity(),
      ...newBot,
    });
  }

  public createBotSettingsForBot(botId: number, settings: IBotSettings) {
    return this._botSettingsRepository.save({
      ...new BotSettingsEntity(),
      ...settings,
      botId,
    });
  }
}
