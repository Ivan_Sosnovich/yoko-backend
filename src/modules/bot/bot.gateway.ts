import {
  SubscribeMessage,
  WebSocketGateway,
  WebSocketServer,
} from "@nestjs/websockets";
import { Server, Socket } from "socket.io";
import { BOT_SETTINGS_WS_EVENT } from "../../shared/constants/wsName";
import { ChangeBotSettingsDto } from "./botSettings/dto";
import { forwardRef, Inject } from "@nestjs/common";
import { TradingMachineService } from "../tradingMachine/tradingMachine.service";

@WebSocketGateway({ cors: true })
export class BotGateway {
  @WebSocketServer()
  server: Server;

  constructor(
    @Inject(forwardRef(() => TradingMachineService))
    private _tradingMachineService: TradingMachineService
  ) {}
  @SubscribeMessage(BOT_SETTINGS_WS_EVENT)
  async handleConnection(client: Socket) {
    const token = client.handshake.headers.authorization;
    if (!token) {
      client.disconnect();
    }
    const validateBot = await this._tradingMachineService._validateBot(
      token.split(" ")[1]
    );
    if (!validateBot) {
      client.disconnect();
    }
    this.server.emit(
      BOT_SETTINGS_WS_EVENT,
      `You are subscribed to the event: ${BOT_SETTINGS_WS_EVENT}`
    );
  }

  sendBotSettings$(
    settings: ChangeBotSettingsDto,
    BOT_ID: number,
    status: string
  ) {
    this.server.emit(BOT_SETTINGS_WS_EVENT, { ...settings, BOT_ID, status });
  }

  sendBotChangeStatus$(BOT_ID: number, status: string) {
    this.server.emit(BOT_SETTINGS_WS_EVENT, { BOT_ID, status });
  }
}
