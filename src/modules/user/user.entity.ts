import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";
import { ColumnNumericTransformer } from "../../shared/lib/ColumnNumericTransformer";
import { USER_TABLE_NAME } from "../../shared/constants/tableName";
import { UserRole } from "../../shared/constants/userRole";
import { IUser } from "./user.types";

@Entity(USER_TABLE_NAME)
export class UserEntity implements IUser {
  @PrimaryGeneratedColumn("increment")
  id: number;

  @Column({ type: "text" })
  name: string;

  @Column({ type: "text" })
  email: string;

  @Column({ type: "text" })
  password: string;

  @Column({ type: "numeric", transformer: new ColumnNumericTransformer() })
  balance: number;

  @Column({ type: "text" })
  referralCode: string;

  @Column({
    type: "numeric",
    nullable: true,
    transformer: new ColumnNumericTransformer(),
  })
  referralId: number;

  @Column({ type: "numeric", transformer: new ColumnNumericTransformer() })
  tradingBalance: number;

  @Column({ type: "boolean" })
  isActive: boolean;

  @Column({ type: "numeric", transformer: new ColumnNumericTransformer() })
  referralBalance: number;

  @Column({
    type: "enum",
    enum: [UserRole.ADMIN, UserRole.USER, UserRole.SUPER_ADMIN],
  })
  role: UserRole;

  @Column({ type: "numeric", transformer: new ColumnNumericTransformer() })
  bonusBalance: number;
  @Column({ type: "text", nullable: true })
  code: string;
}
