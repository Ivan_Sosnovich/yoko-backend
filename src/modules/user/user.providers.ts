import { UserEntity } from "./user.entity";
import { DATABASE_CONNECTION } from "../../database/database.providers";
import { AppDataSource } from "../../database/data-source";
import { USER_TABLE_NAME } from "../../shared/constants/tableName";

export const userProviders = [
  {
    provide: USER_TABLE_NAME,
    useFactory: () => AppDataSource.getRepository(UserEntity),
    inject: [DATABASE_CONNECTION],
  },
];
