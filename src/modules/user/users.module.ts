import { forwardRef, Module } from "@nestjs/common";
import { UsersService } from "./users.service";
import { UsersController } from "./users.controller";
import { userProviders } from "./user.providers";
import { UserRepository } from "./user.repository";
import { DatabaseModule } from "../../database/database.module";
import { UsersGateway } from "./users.gateway";
import { PassportModule } from "@nestjs/passport";
import { JwtModule } from "@nestjs/jwt";
import { LocalStrategy } from "./auth/local.strategy";
import { JwtStrategy } from "./auth/jwt.strategy";
import { PaymentModule } from "../payment/payment.module";
import { MailModule } from "../mailer/mail.module";
import { BotModule } from "../bot/bot.module";
import { ReferralModule } from "./referral/referral.module";

@Module({
  imports: [
    DatabaseModule,
    PassportModule,
    JwtModule.register({
      secret: process.env.JWT_SECRET,
      signOptions: { expiresIn: "15m" },
    }),
    forwardRef(() => PaymentModule),
    forwardRef(() => MailModule),
    forwardRef(() => BotModule),
    forwardRef(() => ReferralModule),
  ],
  providers: [
    ...userProviders,
    UsersService,
    UserRepository,
    UsersGateway,
    LocalStrategy,
    JwtStrategy,
  ],
  controllers: [UsersController],
  exports: [UsersService, UserRepository],
})
export class UsersModule {}
