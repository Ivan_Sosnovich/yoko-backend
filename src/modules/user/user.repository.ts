import { Inject, Injectable } from "@nestjs/common";
import { USER_TABLE_NAME } from "../../shared/constants/tableName";
import { Repository } from "typeorm";
import { UserEntity } from "./user.entity";
import { RegisterUserDTO } from "./dto";
import { UserRole } from "../../shared/constants/userRole";
import { IUser } from "./user.types";

@Injectable()
export class UserRepository {
  constructor(
    @Inject(USER_TABLE_NAME) private _userRepository: Repository<UserEntity>
  ) {}

  public async changeUserTradingBalance(id: number, balance: number) {
    try {
      const user = await this._userRepository.findOneBy({ id });
      if (user) {
        user.tradingBalance = balance;
        await this._userRepository.save(user);
      }
      if (!user) {
        throw Error(`Пользователь по id: ${id}, не найден`);
      }
    } catch (e) {
      throw e.toString();
    }
  }

  public async debitingBalanceForUser(id: number, value: number) {
    try {
      const user = await this._userRepository.findOneBy({ id });
      if (user) {
        const remainsBonusBalance = user.bonusBalance - value;
        user.bonusBalance = remainsBonusBalance > 0 ? remainsBonusBalance : 0;
        if (remainsBonusBalance < 0) {
          user.balance = user.balance + remainsBonusBalance;
        }
        await this._userRepository.save(user);
      }
      if (!user) {
        throw Error(`Пользователь по id: ${id}, не найден`);
      }
    } catch (e) {
      throw e.toString();
    }
  }

  public async updateReferralUserBalanceForUser(id: number, value: number) {
    try {
      const user = await this._userRepository.findOneBy({ id });
      if (user.referralId) {
        const referralUser = await this._userRepository.findOneBy({
          id: user.referralId,
        });
        referralUser.referralBalance = referralUser.referralBalance + value;
        await this._userRepository.save(referralUser);
        return referralUser;
      }
      return null;
    } catch (e) {
      throw e.toString();
    }
  }

  public searchUserForId(id) {
    try {
      return this._userRepository.findOneBy({ id });
    } catch (e) {
      throw e.toString();
    }
  }

  public searchUserForName(login: string) {
    return this._userRepository.findOneBy({ name: login });
  }

  public searchUserForEmail(login: string) {
    return this._userRepository.findOneBy({ email: login });
  }

  public searchUserForReferralCode(referralCode: string) {
    return this._userRepository.findOneBy({ referralCode });
  }

  public createUser(
    registerUserDTO: RegisterUserDTO,
    referralId: number | null
  ) {
    return this._userRepository.save({
      ...new UserEntity(),
      ...registerUserDTO,
      balance: 0,
      tradingBalance: 0,
      bonusBalance: 10,
      isActive: false,
      referralBalance: 0,
      role: UserRole.USER,
      referralId,
    });
  }

  public updateRefCodeForUser(id: number, referralCode: string) {
    return this._userRepository.update(id, { referralCode });
  }

  public updateUserForNewData(id: number, newUserData: Partial<IUser>) {
    return this._userRepository.update(id, { ...newUserData });
  }

  public async depositToUserBalance(id: number, value: number) {
    const user = await this._userRepository.findOneBy({ id });
    if (user) {
      user.balance = +value + +user.balance;
      await this._userRepository.save(user);
    }
    if (!user) {
      throw Error(`Пользователь по id: ${id}, не найден`);
    }
  }

  public searchUserForCode(code: string) {
    return this._userRepository.findOneBy({ code });
  }

  public searchReferralForUser(id: number) {
    return this._userRepository.find({
      where: {
        referralId: id,
      },
    });
  }
}
