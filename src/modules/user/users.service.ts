import { forwardRef, Inject, Injectable, Logger } from "@nestjs/common";
import { UserRepository } from "./user.repository";
import { LoginUserDTO, PasswordRecoveryDTO, RegisterUserDTO } from "./dto";
import { JwtService } from "@nestjs/jwt";
import { IUser, IUserJwt } from "./user.types";
import { getHashPassword, validatePassword } from "./lib/userPassword";
import { getReferralCode } from "./lib/getReferralCode";
import { MailService } from "../mailer/mail.service";
import {
  getCode,
  keyEncryptionByCrypto,
} from "../../shared/lib/keyEncryptionByCrypto";
import * as process from "process";
import { BotService } from "../bot/bot.service";
import { ReferralService } from "./referral/referral.service";
import { IReferral } from "./referral/referral.types";

@Injectable()
export class UsersService {
  private readonly logger = new Logger(UsersService.name);
  constructor(
    private _userRepository: UserRepository,
    private jwtService: JwtService,
    @Inject(forwardRef(() => MailService)) private _mailService: MailService,
    @Inject(forwardRef(() => BotService)) private _botService: BotService,
    private _referralService: ReferralService
  ) {}

  async changeUserTradingBalance(userId: number, balance: number) {
    this.logger.warn(
      `Поступил запрос на изменение торгового баланса пользователя id: ${userId}, на сумму ${balance}`
    );
    try {
      await this._userRepository.changeUserTradingBalance(userId, balance);
    } catch (e) {
      throw e.toString();
    }
  }

  async debitingBalanceForUser(id: number, value: number) {
    return this._userRepository.debitingBalanceForUser(id, value);
  }

  async updateReferralUserBalanceForUser(id: number, value: number) {
    this.logger.log("Обновляем реф баланс пользователя");
    return this._userRepository.updateReferralUserBalanceForUser(id, value);
  }

  async searchUserForId(id: number) {
    return this._userRepository.searchUserForId(id);
  }

  async userLogin(loginUserDTO: LoginUserDTO) {
    try {
      const { login } = loginUserDTO;
      this.logger.warn(`Попытка авторизации пользователя по логину: ${login}`);
      const user = await this._validateUser(loginUserDTO);
      if (!user.isActive) {
        throw "isActive=false";
      }
      if (user) {
        const payload: IUserJwt = {
          id: user.id,
          name: user.name,
          password: user.password,
        };
        const token = this.jwtService.sign(payload);
        return { token };
      }
      if (!user) {
        throw Error("auth.error.ivalid.username.password");
      }
    } catch (e) {
      this.logger.error(
        `При попытки авторизации пользователя, произошла ошибка: ${e}`
      );
      throw e.toString();
    }
  }

  async userRegister(registerUserDTO: RegisterUserDTO) {
    try {
      let referralId: number | null = null;
      this.logger.warn(`Попытка регестрации пользователя`);
      const { name, password, referralCode, email } = registerUserDTO;
      if (!name || !password || !email) {
        throw "Error field is empty";
      }
      if (referralCode) {
        this.logger.warn(
          `При регестрации пользователя, получен реф код: ${referralCode}`
        );
        const userForCode =
          await this._userRepository.searchUserForReferralCode(referralCode);
        if (!userForCode) {
          throw "register.error.referral.code.wrong";
        }
        if (userForCode) {
          referralId = userForCode.id;
        }
      }
      this.logger.log("Поиск пользователя по имени");
      const userForName = await this._userRepository.searchUserForName(name);
      this.logger.log("Поиск пользователя по email");
      const userForEmail = await this._userRepository.searchUserForEmail(email);
      const userBase = userForEmail || userForName;
      if (userBase) {
        throw "register.error.email.login.use";
      }

      const hashPassword = await getHashPassword(password);
      const newRefCode = "test";
      const code = getCode(
        `${registerUserDTO.name}_${registerUserDTO.email}_${registerUserDTO.password}`
      );
      const newUser = await this._userRepository.createUser(
        {
          ...registerUserDTO,
          password: hashPassword,
          referralCode: newRefCode,
          code,
        },
        referralId
      );
      await this._mailService.sendUserConfirmation(
        email,
        `${process.env.API_FRONT_HOST}?code=${code}`
      );
      const refCode = getReferralCode(newUser.id);
      return this._userRepository.updateRefCodeForUser(newUser.id, refCode);
    } catch (e) {
      throw e.toString();
    }
  }

  async resendConfirmationUser(email: string) {
    try {
      this.logger.warn(
        `Поступил запрос, на отправку письма повторно: ${email}`
      );
      const userForEmail = await this._userRepository.searchUserForEmail(email);
      const userForLogin = await this._userRepository.searchUserForName(email);
      const userBase = userForEmail || userForLogin;
      if (!userBase) {
        throw "auth.error.no.user.for.send.login";
      }
      const code = keyEncryptionByCrypto(
        `${userBase.name}_${userBase.email}_${userBase.password}`
      );
      await this._mailService.sendUserConfirmation(
        userBase.email,
        `${process.env.API_FRONT_HOST}?code=${code}`
      );
      await this._userRepository.updateUserForNewData(userBase.id, { code });
    } catch (e) {
      throw e.toString();
    }
  }

  async _validateUser(loginUserDTO: LoginUserDTO) {
    try {
      const { login, password } = loginUserDTO;
      this.logger.log("Поиск пользователя по имени");
      const userForName = await this._userRepository.searchUserForName(login);
      this.logger.log("Поиск пользователя по email");
      const userForEmail = await this._userRepository.searchUserForEmail(login);
      const userBase = userForEmail || userForName;
      if (!userBase) {
        throw "auth.error.ivalid.username.password";
      }
      const validateUserPassword = await validatePassword(
        password,
        userBase.password
      );
      if (!validateUserPassword && password !== userBase.password) {
        console.log("error validate");
        throw "auth.error.ivalid.username.password";
      }
      return userBase;
    } catch (e) {
      throw e.toString();
    }
  }

  async userSession(user: IUser) {
    try {
      const payload: IUserJwt = {
        id: user.id,
        name: user.name,
        password: user.password,
      };
      const token = this.jwtService.sign(payload);
      return { token };
    } catch (e) {
      throw e.toString();
    }
  }

  async getUserData(user: IUser) {
    try {
      const { id } = user;
      const {
        name,
        email,
        balance,
        tradingBalance,
        referralCode,
        isActive,
        referralBalance,
        role,
        bonusBalance,
      } = await this._userRepository.searchUserForId(id);

      return {
        id,
        name,
        email,
        balance: balance + bonusBalance,
        tradingBalance,
        referralCode,
        isActive,
        referralBalance,
        role,
      };
    } catch (e) {
      throw e.toString();
    }
  }

  async transferRefBalanceToUserBalance(id: number, value: number) {
    try {
      this.logger.warn(
        `Посупил запрос на перевод средств с реф баланса на баланс пользователя по id: ${id}, на сумму ${value}`
      );
      const { referralBalance, balance } =
        await this._userRepository.searchUserForId(id);
      const newReferralBalance = referralBalance - value;
      if (newReferralBalance < 0) {
        throw Error("Реферальный баланс не может быть меньше 0");
      }
      if (newReferralBalance >= 0) {
        await this._userRepository.updateUserForNewData(id, {
          balance: balance + value,
          referralBalance: newReferralBalance,
        });
      }
    } catch (e) {
      this.logger.error(
        `При переводе средств с реф баланса на основной для пользователя по id: ${id}, произошла ошибка: ${e.toString()}`
      );
      throw e.toString();
    }
  }

  async depositToUserBalance(id: number, value: number) {
    try {
      return this._userRepository.depositToUserBalance(id, value);
    } catch (e) {
      throw e.toString();
    }
  }

  async userConfirmation(code: string) {
    try {
      this.logger.warn(`Запрос на верфификацию пользователя по коду: ${code}`);
      const user = await this._userRepository.searchUserForCode(code);
      if (!user) {
        throw Error("Пользователь по коду не найден");
      }
      if (user.isActive) {
        throw Error("Пользователь уже активирован");
      }
      await this._userRepository.updateUserForNewData(user.id, {
        isActive: true,
      });
      await this._botService.createBot(user.id);
    } catch (e) {
      this.logger.error(
        `При верификации пользователя произошла ошибка: ${e.toString()}`
      );
      throw e.toString();
    }
  }

  async sendEmailForPasswordRecovery(email: string) {
    try {
      this.logger.warn(`Поступил запрос на сброс пароля по email: ${email}`);
      const userForEmail = await this._userRepository.searchUserForEmail(email);
      const userForLogin = await this._userRepository.searchUserForName(email);
      const userBase = userForEmail || userForLogin;
      if (!userBase) {
        throw "auth.error.no.user.for.send.login";
      }
      const code = keyEncryptionByCrypto(
        `${userBase.name}_${userBase.email}_${userBase.password}`
      );
      await this._mailService.sendEmailForPasswordRecovery(
        userBase.email,
        `${process.env.API_FRONT_HOST}?recoveryCode=${code}`
      );
      await this._userRepository.updateUserForNewData(userBase.id, { code });
    } catch (e) {
      this.logger.error(
        `Произошла ошибка при сбросе пароля пользователя по email: ${email}`
      );
      throw e.toString();
    }
  }

  async passwordRecovery(passwordRecoveryDTO: PasswordRecoveryDTO) {
    try {
      const { recoveryCode, password } = passwordRecoveryDTO;
      this.logger.warn(
        `Поступил запрос на сброс пароля по коду: ${recoveryCode}`
      );
      const user = await this._userRepository.searchUserForCode(recoveryCode);
      if (!user) {
        throw Error(`По коду: ${recoveryCode}, пользователь не найден`);
      }
      const newPassword = await getHashPassword(password);
      await this._userRepository.updateUserForNewData(user.id, {
        password: newPassword,
      });
    } catch (e) {
      this.logger.error(
        `При сбросе пароля пользователя по коду: ${
          passwordRecoveryDTO.recoveryCode
        } произошла ошибка: ${e.toString()}`
      );
      throw e.toString();
    }
  }
  async createReferralForUser(
    userId: number,
    referralData: Omit<IReferral, "date">
  ) {
    return this._referralService.createReferralForUser(userId, referralData);
  }

  async searchReferralForUser(id: number) {
    try {
      this.logger.warn(
        `Поступил запрос на количество рефералов для пользователя: ${id}`
      );
      const referralForUser = await this._userRepository.searchReferralForUser(
        id
      );
      return {
        count: referralForUser?.length,
      };
    } catch (e: any) {
      throw e.toString();
    }
  }
}
