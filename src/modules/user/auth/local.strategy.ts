import { Strategy } from "passport-local";
import { PassportStrategy } from "@nestjs/passport";
import { Injectable, UnauthorizedException } from "@nestjs/common";
import { UsersService } from "../users.service";

@Injectable()
export class LocalStrategy extends PassportStrategy(Strategy) {
  constructor(private _usersService: UsersService) {
    super();
  }

  async validate(login: string, password: string): Promise<any> {
    const user = await this._usersService._validateUser({ login, password });
    if (!user) {
      throw new UnauthorizedException();
    }
    return user;
  }
}
