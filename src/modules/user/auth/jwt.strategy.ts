import { ExtractJwt, Strategy } from "passport-jwt";
import { PassportStrategy } from "@nestjs/passport";
import { Injectable, UnauthorizedException } from "@nestjs/common";
import { UsersService } from "../users.service";
import { IUserJwt } from "../user.types";

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(private _usersService: UsersService) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: process.env.JWT_SECRET,
    });
  }

  async validate({ name, password }: IUserJwt) {
    const user = await this._usersService._validateUser({
      login: name,
      password,
    });
    if (!user) {
      throw new UnauthorizedException();
    }
    return user;
  }
}
