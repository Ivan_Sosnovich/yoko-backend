import * as bcrypt from "bcrypt";

const solt = 10;

export const getHashPassword = async (password: string): Promise<string> => {
  return await bcrypt.hash(password, solt);
};

export const validatePassword = async (
  password: string,
  hashPassword: string
): Promise<boolean> => {
  return await bcrypt.compare(password, hashPassword);
};
