import { referalCodeData } from "./data";
export const getReferralCode = (id: number) => {
  return referalCodeData.split(" ")[id];
};
