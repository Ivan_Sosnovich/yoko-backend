let count = 0;
const refCodeSet = new Set();
let fileData = "";
while (count <= 100000) {
  const code = generateCode();
  if (!refCodeSet.has(code)) {
    refCodeSet.add(code);
    count++;
    fileData += code + " ";
  }
}

function generateCode() {
  const arr_en = [
    "a",
    "b",
    "c",
    "d",
    "e",
    "f",
    "g",
    "h",
    "i",
    "j",
    "k",
    "l",
    "m",
    "n",
    "o",
    "p",
    "q",
    "r",
    "s",
    "t",
    "u",
    "v",
    "w",
    "x",
    "y",
    "z",
  ];
  const arr_EN = [
    "A",
    "B",
    "C",
    "D",
    "E",
    "F",
    "G",
    "H",
    "I",
    "J",
    "K",
    "L",
    "M",
    "N",
    "O",
    "P",
    "Q",
    "R",
    "S",
    "T",
    "U",
    "V",
    "W",
    "X",
    "Y",
    "Z",
  ];
  const arr_num = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0];
  const randomArr = [arr_en, arr_num, arr_EN, arr_en, arr_num];
  let count = 0;
  let code = "";
  while (count < randomArr.length) {
    const indexR = Math.floor(Math.random() * randomArr.length);
    const symboleR = Math.floor(Math.random() * randomArr[indexR].length);
    code += randomArr[indexR][symboleR];
    count++;
  }
  return code;
}

console.log(fileData)