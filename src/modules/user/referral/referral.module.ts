import { forwardRef, Module } from "@nestjs/common";
import { referralProviders } from "./referral.providers";
import { DatabaseModule } from "../../../database/database.module";
import { ReferralService } from "./referral.service";
import { ReferralRepository } from "./referral.repository";
import { UsersModule } from "../users.module";

@Module({
  imports: [DatabaseModule, forwardRef(() => UsersModule)],
  providers: [...referralProviders, ReferralService, ReferralRepository],
  controllers: [],
  exports: [ReferralService, ReferralRepository],
})
export class ReferralModule {}
