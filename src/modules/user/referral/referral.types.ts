import { IUser } from "../user.types";

export interface IReferral {
  id?: number;
  referralUser: IUser;
  userId: number;
  profit: number;
  date: string;
}
