import {
  Column,
  Entity,
  JoinColumn,
  OneToOne,
  PrimaryGeneratedColumn,
  CreateDateColumn,
} from "typeorm";
import { ColumnNumericTransformer } from "../../../shared/lib/ColumnNumericTransformer";
import { IReferral } from "./referral.types";
import { UserEntity } from "../user.entity";
import { REFERRAL_TABLE_NAME } from "../../../shared/constants/tableName";
@Entity(REFERRAL_TABLE_NAME)
export class ReferralEntity implements IReferral {
  @PrimaryGeneratedColumn("increment")
  id: number;

  @CreateDateColumn({ name: "date" })
  date: string;

  @Column({ type: "numeric", transformer: new ColumnNumericTransformer() })
  profit: number;

  @OneToOne(() => UserEntity, (user) => user.id, {
    cascade: true,
    eager: true,
  })
  @JoinColumn({ name: "referralUser" })
  referralUser: UserEntity;

  @Column({ type: "numeric" })
  userId: number;
}
