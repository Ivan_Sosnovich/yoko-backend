import { DATABASE_CONNECTION } from "../../../database/database.providers";
import { AppDataSource } from "../../../database/data-source";
import { ReferralEntity } from "./referral.entity";
import { REFERRAL_TABLE_NAME } from "../../../shared/constants/tableName";
export const referralProviders = [
  {
    provide: REFERRAL_TABLE_NAME,
    useFactory: () => AppDataSource.getRepository(ReferralEntity),
    inject: [DATABASE_CONNECTION],
  },
];
