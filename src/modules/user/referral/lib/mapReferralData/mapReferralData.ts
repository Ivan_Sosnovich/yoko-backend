import { GetReferralDataDTO } from "src/modules/user/dto";
import { ReferralEntity } from "../../referral.entity";

export const mapReferralData = (
  data: ReferralEntity[]
): GetReferralDataDTO[] => {
  return data.map(({ id, referralUser: { name }, profit, date }) => ({
    id,
    referralUser: name,
    profit,
    date,
  }));
};
