import { forwardRef, Inject, Injectable, Logger } from "@nestjs/common";
import { IReferral } from "./referral.types";
import { UsersService } from "../users.service";
import { ReferralRepository } from "./referral.repository";
import { mapReferralData } from "./lib/mapReferralData";

@Injectable()
export class ReferralService {
  private readonly logger = new Logger(ReferralService.name);

  constructor(
    @Inject(forwardRef(() => UsersService)) private _userService: UsersService,
    private _referralRepository: ReferralRepository
  ) {}

  async createReferralForUser(
    referralId: number,
    referralData: Omit<IReferral, "date">
  ) {
    try {
      const user = await this._userService.searchUserForId(referralId);
      await this._referralRepository.createReferralForUser({
        ...referralData,
        referralUser: user,
      });
    } catch (e) {
      throw e.toString();
    }
  }
  async getReferralUserData(id: number, count: number, pageNumber: number) {
    try {
      this.logger.warn(
        `Поступил запрос на поиск данных реферала по id: ${id}, количество элементов на странице: ${count}, номер страницы: ${pageNumber}`
      );
      const [data, referralCount] =
        await this._referralRepository.getReferralUserData(
          id,
          count,
          pageNumber
        );

      return {
        data: mapReferralData(data),
        count: Math.ceil(referralCount / count),
      };
    } catch (e) {
      this.logger.error(
        `При запросе на поиск данных реферала по id: ${id}, количество элементов на странице: ${count}, номер страницы: ${pageNumber}, произошла ошибка: ${e.toString()}`
      );
      throw e.toString();
    }
  }
}
