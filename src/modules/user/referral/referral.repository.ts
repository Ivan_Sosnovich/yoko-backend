import { Injectable, Inject } from "@nestjs/common";
import { REFERRAL_TABLE_NAME } from "../../../shared/constants/tableName";
import { Repository } from "typeorm";
import { ReferralEntity } from "./referral.entity";
import { IReferral } from "./referral.types";
@Injectable()
export class ReferralRepository {
  constructor(
    @Inject(REFERRAL_TABLE_NAME)
    private _referralRepository: Repository<ReferralEntity>
  ) {}

  public createReferralForUser(referralData: Omit<IReferral, "date">) {
    return this._referralRepository.save({
      ...new ReferralEntity(),
      ...referralData,
    });
  }

  public getReferralUserData(id: number, count: number, pageNumber: number) {
    return this._referralRepository.findAndCount({
      where: {
        userId: id,
      },
      take: count,
      skip: count * pageNumber,
      order: {
        date: "DESC",
      },
    });
  }
}
