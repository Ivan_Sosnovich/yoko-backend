import { ApiProperty } from "@nestjs/swagger";

export class ResponseUserAuthDTO {
  @ApiProperty()
  token: string;
}
