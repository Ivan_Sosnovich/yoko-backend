import { ApiProperty } from "@nestjs/swagger";
import { IsString, IsNotEmpty, IsEmail, IsOptional } from "class-validator";

export class RegisterUserDTO {
  @ApiProperty({ required: true })
  @IsString()
  @IsNotEmpty()
  name: string;
  @ApiProperty({ required: true })
  @IsEmail()
  @IsNotEmpty()
  email: string;
  @ApiProperty({ required: true })
  @IsString()
  @IsNotEmpty()
  password: string;
  @ApiProperty({ required: false })
  @IsString()
  @IsOptional()
  referralCode: string;
  @IsString()
  @IsOptional()
  code: string;
}
