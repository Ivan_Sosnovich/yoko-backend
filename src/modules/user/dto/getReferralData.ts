import { ApiProperty } from "@nestjs/swagger";
export class GetReferralDataDTO {
  @ApiProperty({ required: true })
  id?: number;
  @ApiProperty({ required: true })
  referralUser: string;
  @ApiProperty({ required: true })
  profit: number;
  @ApiProperty({ required: true })
  date: string;
}

export class GetReferralResponceDTO {
  @ApiProperty({ required: true })
  data: GetReferralDataDTO;
  @ApiProperty({ required: true })
  count: number;
}

export class GetReferralCountDTO {
  @ApiProperty({ required: true })
  count: number;
}
