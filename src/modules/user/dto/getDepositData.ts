import { ApiProperty } from "@nestjs/swagger";
import { DepositStatus } from "../../payment/deposit/deposit.types";

export class GetTransactionsDTO {
  @ApiProperty({ required: true })
  id?: number;
  @ApiProperty({ required: true })
  status: string;
  @ApiProperty({ required: true })
  network: string;
  @ApiProperty({ required: true })
  currency: string;
  @ApiProperty({ required: true })
  amount: string;
  @ApiProperty({ required: true })
  tx: string;
  @ApiProperty({ required: true })
  sender: string;
  @ApiProperty({ required: true })
  confirmations: string;
}

export class GetDepositDataDTO {
  @ApiProperty({ required: true })
  id?: number;
  @ApiProperty({ required: true })
  userId: number;
  @ApiProperty({ required: true })
  status: DepositStatus;
  @ApiProperty({ required: true })
  amount: string;
  @ApiProperty({ required: true })
  createAt: string;
  @ApiProperty({ required: true })
  closedAt: string;
  @ApiProperty({ required: true })
  description?: string;
  @ApiProperty({ required: true })
  network: string;
  @ApiProperty({ required: true })
  currency: string;
  @ApiProperty({ required: true })
  advancedBalanceId: string;
  @ApiProperty({ required: true })
  address?: string;
  @ApiProperty({ required: true })
  received?: string;
  @ApiProperty({ required: true, isArray: true })
  transactionsIds?: GetTransactionsDTO;
  @ApiProperty({ required: true })
  paymentOrderID?: string;
  @ApiProperty({ required: true })
  addressId?: string;
  @ApiProperty({ required: true })
  link?: string;
  @ApiProperty({ required: true })
  clientOrderId?: string;
  @ApiProperty({ required: true })
  expiresAt?: string;
}

export class GetDepositDTO {
  @ApiProperty({ required: true })
  data: GetDepositDataDTO;
  @ApiProperty({ required: true })
  count: number;
}
