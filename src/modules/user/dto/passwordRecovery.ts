import { ApiProperty } from "@nestjs/swagger";
import { IsString, IsNotEmpty } from "class-validator";

export class PasswordRecoveryDTO {
  @ApiProperty({ required: true })
  @IsString()
  @IsNotEmpty()
  recoveryCode: string;
  @ApiProperty({ required: true })
  @IsString()
  @IsNotEmpty()
  password: string;
}
