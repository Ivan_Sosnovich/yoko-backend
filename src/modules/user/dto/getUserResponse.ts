import { ApiProperty } from "@nestjs/swagger";
import { UserRole } from "../../../shared/constants/userRole";

export class GetUserResponce {
  @ApiProperty({ required: true })
  id: number;
  @ApiProperty({ required: true })
  name: string;
  @ApiProperty({ required: true })
  email: string;
  @ApiProperty({ required: true })
  balance: number;
  @ApiProperty({ required: true })
  tradingBalance: number;
  @ApiProperty({ required: true })
  referralCode: string;
  @ApiProperty({ required: true })
  isActive: boolean;
  @ApiProperty({ required: true })
  referralBalance: number;
  @ApiProperty({ required: true })
  role: UserRole;
}
