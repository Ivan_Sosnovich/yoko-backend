import { ApiProperty } from "@nestjs/swagger";
import { IsNumber, IsNotEmpty, IsPositive } from "class-validator";

export class TransferReferralBalanceToUserBalanceDTO {
  @ApiProperty({ required: true })
  @IsNumber()
  @IsNotEmpty()
  @IsPositive()
  deposit: number;
}
