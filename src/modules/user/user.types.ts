import { UserRole } from "../../shared/constants/userRole";

export type IUser = {
  id?: number;
  name: string;
  email: string;
  password: string;
  balance: number;
  tradingBalance: number;
  referralCode: string;
  referralId?: number;
  isActive: boolean;
  referralBalance: number;
  role: UserRole;
  bonusBalance: number;
  code?: string;
};

export type IUserJwt = Pick<IUser, "id" | "name" | "password"> & {
  iat?: number;
  exp?: number;
  id: number;
};
