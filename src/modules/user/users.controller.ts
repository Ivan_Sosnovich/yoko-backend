import {
  Controller,
  Post,
  Body,
  Res,
  Get,
  UseGuards,
  Req,
  Param,
  Inject,
  forwardRef,
} from "@nestjs/common";
import {
  ApiTags,
  ApiBody,
  ApiResponse,
  ApiHeader,
  ApiParam,
} from "@nestjs/swagger";
import { JwtAuthGuard } from "../../guards/JwtGuard";
import {
  GetDepositDTO,
  GetReferralCountDTO,
  GetReferralResponceDTO,
  GetUserResponce,
  LoginUserDTO,
  PasswordRecoveryDTO,
  RegisterUserDTO,
  ResponseUserAuthDTO,
  TransferReferralBalanceToUserBalanceDTO,
} from "./dto";
import {
  ResponseError,
  ResponseSuccess,
} from "../../shared/types/server/response_error";
import { Response, Request } from "express";
import { UsersService } from "./users.service";
import { HttpStatus } from "../../shared/types/server/http_status";
import { IUser } from "./user.types";
import { ReferralService } from "./referral/referral.service";
import { DepositService } from "../payment/deposit/deposit.service";

@ApiTags("user controller")
@Controller("user")
export class UsersController {
  constructor(
    private _userService: UsersService,
    private _referralService: ReferralService,
    @Inject(forwardRef(() => DepositService))
    private _depositService: DepositService
  ) {}

  @Post("/login")
  @ApiBody({ type: LoginUserDTO })
  @ApiResponse({ status: 200, type: ResponseUserAuthDTO })
  @ApiResponse({
    status: 404,
    description: "Ошибки логирования пользователя",
    type: ResponseError,
  })
  userLogin(@Body() loginUserDTO: LoginUserDTO, @Res() response: Response) {
    this._userService
      .userLogin(loginUserDTO)
      .then((token) => response.status(200).send(token))
      .catch((e) =>
        response.status(HttpStatus.NOT_FOUND).send({
          status: HttpStatus.NOT_FOUND,
          message: e,
        })
      );
  }

  @Post("/register")
  @ApiBody({ type: RegisterUserDTO })
  @ApiResponse({ status: 200, type: ResponseSuccess })
  @ApiResponse({
    status: 404,
    description: "Ошибки регестрации пользователя",
    type: ResponseError,
  })
  userRegister(
    @Body() registerUserDTO: RegisterUserDTO,
    @Res() response: Response
  ) {
    this._userService
      .userRegister(registerUserDTO)
      .then(() => response.status(200).send({ message: "success" }))
      .catch((e) =>
        response.status(HttpStatus.BAD_REQUEST).send({
          status: HttpStatus.BAD_REQUEST,
          message: e.toString(),
        })
      );
  }

  @UseGuards(JwtAuthGuard)
  @Get("/session")
  @ApiHeader({
    name: "Authorization",
    required: true,
    description: "Bearer token",
  })
  @ApiResponse({ status: 200, type: ResponseUserAuthDTO })
  @ApiResponse({
    status: 401,
    description: "Ошибка авторизации",
    type: ResponseError,
  })
  userSession(@Req() request: Request, @Res() response: Response) {
    const user = request.user as IUser;
    this._userService
      .userSession(user)
      .then((token) => response.status(200).send(token))
      .catch((e) =>
        response.status(HttpStatus.BAD_REQUEST).send({
          status: HttpStatus.BAD_REQUEST,
          message: e.toString(),
        })
      );
  }

  @UseGuards(JwtAuthGuard)
  @Get("")
  @ApiHeader({
    name: "Authorization",
    required: true,
    description: "Bearer token",
  })
  @ApiResponse({ status: 200, type: GetUserResponce })
  @ApiResponse({
    status: 401,
    description: "Ошибка авторизации",
    type: ResponseError,
  })
  getUser(@Req() request: Request, @Res() response: Response) {
    const user = request.user as IUser;
    this._userService
      .getUserData(user)
      .then((data) => response.status(200).send(data))
      .catch((e) =>
        response.status(HttpStatus.BAD_REQUEST).send({
          status: HttpStatus.BAD_REQUEST,
          message: e.toString(),
        })
      );
  }

  @UseGuards(JwtAuthGuard)
  @Post("/transfer/referral/balance")
  @ApiBody({ type: TransferReferralBalanceToUserBalanceDTO })
  @ApiHeader({
    name: "Authorization",
    required: true,
    description: "Bearer token",
  })
  @ApiResponse({ status: 200, type: ResponseSuccess })
  @ApiResponse({
    status: 401,
    description: "Ошибка авторизации",
    type: ResponseError,
  })
  @ApiResponse({
    status: 404,
    description: "Ошибки перевода с реф баланса на баланс пользователя",
    type: ResponseError,
  })
  transferRefBalanceToUserBalance(
    @Body()
    transferReferralBalanceToUserBalanceDTO: TransferReferralBalanceToUserBalanceDTO,
    @Req() request: Request,
    @Res() response: Response
  ) {
    const { id } = request.user as IUser;
    const { deposit } = transferReferralBalanceToUserBalanceDTO;
    this._userService
      .transferRefBalanceToUserBalance(id, deposit)
      .then(() => response.status(200).send({ message: "success" }))
      .catch((e) =>
        response.status(HttpStatus.BAD_REQUEST).send({
          status: HttpStatus.BAD_REQUEST,
          message: e.toString(),
        })
      );
  }

  @UseGuards(JwtAuthGuard)
  @Get("/referral/:count/:pageNumber")
  @ApiHeader({
    name: "Authorization",
    required: true,
    description: "Bearer token",
  })
  @ApiParam({
    name: "pageNumber",
    description: "Номер страницы для отображения",
    schema: { type: "number" },
  })
  @ApiResponse({
    status: 200,
    type: GetReferralResponceDTO,
  })
  @ApiResponse({
    status: 401,
    description: "Ошибка авторизации",
    type: ResponseError,
  })
  @ApiResponse({
    status: 404,
    description: "Ошибки получения реф таблицы пользователя",
    type: ResponseError,
  })
  getReferralCountForUser(
    @Req() request: Request,
    @Res() response: Response,
    @Param() { count, pageNumber }
  ) {
    const { id } = request.user as IUser;
    this._referralService
      .getReferralUserData(id, count, pageNumber)
      .then((data) => response.status(200).send(data))
      .catch((e) =>
        response.status(HttpStatus.BAD_REQUEST).send({
          status: HttpStatus.BAD_REQUEST,
          message: e.toString(),
        })
      );
  }

  @UseGuards(JwtAuthGuard)
  @Get("/referral/count")
  @ApiHeader({
    name: "Authorization",
    required: true,
    description: "Bearer token",
  })
  @ApiResponse({
    status: 200,
    type: GetReferralCountDTO,
  })
  @ApiResponse({
    status: 401,
    description: "Ошибка авторизации",
    type: ResponseError,
  })
  @ApiResponse({
    status: 404,
    description: "Ошибки получения реф таблицы пользователя",
    type: ResponseError,
  })
  getReferralUserData(@Req() request: Request, @Res() response: Response) {
    const { id } = request.user as IUser;
    this._userService
      .searchReferralForUser(id)
      .then((data) => response.status(200).send(data))
      .catch((e) =>
        response.status(HttpStatus.BAD_REQUEST).send({
          status: HttpStatus.BAD_REQUEST,
          message: e.toString(),
        })
      );
  }

  @UseGuards(JwtAuthGuard)
  @Get("/deposit/:count/:pageNumber")
  @ApiHeader({
    name: "Authorization",
    required: true,
    description: "Bearer token",
  })
  @ApiParam({
    name: "count",
    description: "Количество строк для отображения на странице",
    schema: { type: "number" },
  })
  @ApiParam({
    name: "pageNumber",
    description: "Номер страницы для отображения",
    schema: { type: "number" },
  })
  @ApiResponse({
    status: 200,
    type: GetDepositDTO,
  })
  @ApiResponse({
    status: 401,
    description: "Ошибка авторизации",
    type: ResponseError,
  })
  @ApiResponse({
    status: 404,
    description: "Ошибки получения реф таблицы пользователя",
    type: ResponseError,
  })
  getDepositUserData(
    @Req() request: Request,
    @Res() response: Response,
    @Param() { count, pageNumber }
  ) {
    const { id } = request.user as IUser;
    this._depositService
      .getDepositUserData(id, count, pageNumber)
      .then((data) => response.status(200).send(data))
      .catch((e) =>
        response.status(HttpStatus.BAD_REQUEST).send({
          status: HttpStatus.BAD_REQUEST,
          message: e.toString(),
        })
      );
  }

  @Get("/confirmation/:code")
  @ApiResponse({ status: 200, type: ResponseSuccess })
  @ApiResponse({
    status: 404,
    description: "Ошибка верфификации пользователя",
    type: ResponseError,
  })
  @ApiParam({
    name: "code",
    schema: { type: "string" },
  })
  userConfirmation(@Param() { code }, @Res() response: Response) {
    this._userService
      .userConfirmation(code)
      .then(() => response.status(200).send({ message: "success" }))
      .catch((e) =>
        response.status(HttpStatus.BAD_REQUEST).send({
          status: HttpStatus.BAD_REQUEST,
          message: e.toString(),
        })
      );
  }

  @Get("/password/recovery/:email")
  @ApiResponse({ status: 200, type: ResponseSuccess })
  @ApiResponse({
    status: 404,
    description: "Ошибка отправки письма для сброса пользователя",
    type: ResponseError,
  })
  @ApiParam({
    name: "email",
    schema: { type: "string" },
  })
  sendEmailForPasswordRecovery(@Param() { email }, @Res() response: Response) {
    this._userService
      .sendEmailForPasswordRecovery(email)
      .then(() => response.status(200).send({ message: "success" }))
      .catch((e) =>
        response.status(HttpStatus.BAD_REQUEST).send({
          status: HttpStatus.BAD_REQUEST,
          message: e.toString(),
        })
      );
  }

  @Get("/confirmation/resend/:email")
  @ApiResponse({ status: 200, type: ResponseSuccess })
  @ApiResponse({
    status: 404,
    type: ResponseError,
  })
  @ApiParam({
    name: "email",
    schema: { type: "string" },
  })
  resendConfirmationUser(@Param() { email }, @Res() response: Response) {
    this._userService
      .resendConfirmationUser(email)
      .then(() => response.status(200).send({ message: "success" }))
      .catch((e) =>
        response.status(HttpStatus.BAD_REQUEST).send({
          status: HttpStatus.BAD_REQUEST,
          message: e.toString(),
        })
      );
  }

  @Post("/password/recovery")
  @ApiBody({
    type: PasswordRecoveryDTO,
  })
  @ApiResponse({ status: 200, type: ResponseSuccess })
  @ApiResponse({
    status: 404,
    description: "Ошибка сброса пароля пользователя",
    type: ResponseError,
  })
  passwordRecovery(
    @Body() passwordRecoveryDTO: PasswordRecoveryDTO,
    @Res() response: Response
  ) {
    this._userService
      .passwordRecovery(passwordRecoveryDTO)
      .then(() => response.status(200).send({ message: "success" }))
      .catch((e) =>
        response.status(HttpStatus.BAD_REQUEST).send({
          status: HttpStatus.BAD_REQUEST,
          message: e.toString(),
        })
      );
  }
}
