import * as dotenv from "dotenv";
import * as path from "path";
import { DataSource } from "typeorm";
import { migrations } from "./migration";
dotenv.config({ path: ".env" });

export const AppDataSource = new DataSource({
  type: "postgres",
  host: process.env.POSTGRES_HOST,
  port: Number(process.env.POSTGRES_PORT) || 5432,
  username: process.env.POSTGRES_USER,
  password: process.env.POSTGRES_PASSWORD,
  database: process.env.POSTGRES_DATABASE,
  entities: [path.join(__dirname, "../**/*.entity{.ts,.js}")],
  migrations: migrations,
  synchronize: false,
  logging: false,
  cache: true,
});
