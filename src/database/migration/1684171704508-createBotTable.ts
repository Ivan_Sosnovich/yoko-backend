import { MigrationInterface, QueryRunner, Table } from "typeorm";
import { BOT_TABLE_NAME } from "../../shared/constants/tableName";
import { BotStatus } from "../../shared/constants/botStatus";
export class createBotTable1684171704508 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: BOT_TABLE_NAME,
        columns: [
          {
            name: "id",
            type: "int",
            isGenerated: true,
            generationStrategy: "increment",
            isPrimary: true,
          },
          {
            name: "login",
            type: "text",
          },
          {
            name: "password",
            type: "text",
          },
          {
            name: "isCreated",
            type: "boolean",
          },
          {
            name: "status",
            type: "enum",
            enum: [
              BotStatus.CREATE,
              BotStatus.OFF,
              BotStatus.ON,
              BotStatus.ERROR,
            ],
          },
          {
            name: "userId",
            type: "numeric",
            isNullable: true,
          },
          {
            name: "error",
            type: "numeric",
            isNullable: true,
          },
        ],
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable(BOT_TABLE_NAME);
  }
}
