import { MigrationInterface, QueryRunner, Table } from "typeorm";
import { POSITION_TABLE_NAME } from "../../shared/constants/tableName";
import { PositionDirection } from "../../shared/constants/positionDirection";

export class createPositionTable1684254023768 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: POSITION_TABLE_NAME,
        columns: [
          {
            name: "id",
            type: "int",
            isGenerated: true,
            generationStrategy: "increment",
            isPrimary: true,
          },
          {
            name: "symbol",
            type: "text",
          },
          {
            name: "isOpen",
            type: "boolean",
          },
          {
            name: "direction",
            type: "enum",
            enum: [
              PositionDirection.USER,
              PositionDirection.SELL,
              PositionDirection.BUY,
            ],
          },
          {
            name: "size",
            type: "numeric",
          },
          {
            name: "openingPrice",
            type: "numeric",
          },
          {
            name: "closingPrice",
            type: "numeric",
            isNullable: true,
          },
          {
            name: "openingTime",
            type: "timestamp without time zone",
            default: "now()",
          },
          {
            name: "closingTime",
            type: "timestamp without time zone",
            isNullable: true,
            default: "now()",
          },
          {
            name: "commissions",
            type: "numeric",
            isNullable: true,
          },
          {
            name: "targetPrice",
            type: "numeric",
          },
          {
            name: "parentPositionId",
            type: "numeric",
            isNullable: true,
          },
          {
            name: "isParentPositions",
            type: "boolean",
          },
          {
            name: "profit",
            type: "numeric",
            isNullable: true,
          },
          {
            name: "userId",
            type: "numeric",
          },
        ],
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable(POSITION_TABLE_NAME);
  }
}
