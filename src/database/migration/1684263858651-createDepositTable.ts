import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableColumn,
  TableForeignKey,
} from "typeorm";
import {
  DEPOSIT_TABLE_NAME,
  USER_TABLE_NAME,
} from "../../shared/constants/tableName";

export class createDepositTable1684263858651 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: DEPOSIT_TABLE_NAME,
        columns: [
          {
            name: "id",
            type: "int",
            isGenerated: true,
            generationStrategy: "increment",
            isPrimary: true,
          },
          {
            name: "status",
            type: "enum",
            enum: [
              "init",
              "error",
              "processed",
              "pending",
              "expired",
              "partial",
            ],
          },
          {
            name: "amount",
            type: "text",
          },
          {
            name: "createAt",
            type: "timestamp without time zone",
            default: "now()",
          },
          {
            name: "closedAt",
            type: "timestamp without time zone",
            default: "now()",
          },
          {
            name: "description",
            type: "text",
            isNullable: true,
          },
          {
            name: "network",
            type: "text",
          },
          {
            name: "currency",
            type: "text",
          },
          {
            name: "advancedBalanceId",
            type: "text",
          },
          {
            name: "address",
            type: "text",
            isNullable: true,
          },
          {
            name: "received",
            type: "text",
          },
          {
            name: "paymentOrderID",
            type: "text",
            isNullable: true,
          },
          {
            name: "addressId",
            type: "text",
            isNullable: true,
          },
          {
            name: "link",
            type: "text",
            isNullable: true,
          },
          {
            name: "clientOrderId",
            type: "text",
            isNullable: true,
          },
          {
            name: "expiresAt",
            type: "text",
            isNullable: true,
          },
        ],
      })
    );

    await queryRunner.addColumn(
      DEPOSIT_TABLE_NAME,
      new TableColumn({
        name: "userId",
        type: "int",
        isNullable: true,
      })
    );

    await queryRunner.createForeignKey(
      DEPOSIT_TABLE_NAME,
      new TableForeignKey({
        columnNames: ["userId"],
        referencedColumnNames: ["id"],
        referencedTableName: USER_TABLE_NAME,
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    const table = await queryRunner.getTable(DEPOSIT_TABLE_NAME);
    const foreignKey = table.foreignKeys.find(
      (fk) => fk.columnNames.indexOf("userId") !== -1
    );
    await queryRunner.dropForeignKey(DEPOSIT_TABLE_NAME, foreignKey);
    await queryRunner.dropColumn(DEPOSIT_TABLE_NAME, "userId");
    await queryRunner.dropTable(DEPOSIT_TABLE_NAME);
  }
}
