import { MigrationInterface, QueryRunner, Table } from "typeorm";
import { USER_TABLE_NAME } from "../../shared/constants/tableName";
import { UserRole } from "../../shared/constants/userRole";

export class createUserTable1684159042726 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: USER_TABLE_NAME,
        columns: [
          {
            name: "id",
            type: "int",
            isGenerated: true,
            generationStrategy: "increment",
            isPrimary: true,
          },
          {
            name: "name",
            type: "text",
          },
          {
            name: "email",
            type: "text",
          },
          {
            name: "password",
            type: "text",
          },
          {
            name: "balance",
            type: "numeric",
          },
          {
            name: "tradingBalance",
            type: "numeric",
          },
          {
            name: "referralCode",
            type: "text",
          },
          {
            name: "referralId",
            type: "numeric",
            isNullable: true,
          },
          {
            name: "isActive",
            type: "boolean",
          },
          {
            name: "referralBalance",
            type: "numeric",
          },
          {
            name: "role",
            type: "enum",
            enum: [UserRole.ADMIN, UserRole.USER, UserRole.SUPER_ADMIN],
          },
          {
            name: "bonusBalance",
            type: "numeric",
          },
          { name: "code", type: "text", isNullable: true },
        ],
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable(USER_TABLE_NAME);
  }
}
