import { createUserTable1684159042726 } from "./1684159042726-createUserTable";
import { createBotTable1684171704508 } from "./1684171704508-createBotTable";
import { createBotSettingsTable1684247276303 } from "./1684247276303-createBotSettingsTable";
import { createPositionTable1684254023768 } from "./1684254023768-createPositionTable";
import { createReferralTable1684260002994 } from "./1684260002994-createReferralTable";
import { createDepositTable1684263858651 } from "./1684263858651-createDepositTable";
import { createTransactionsTable1684264969919 } from "./1684264969919-createTransactionsTable";
export const migrations = [
  createUserTable1684159042726,
  createBotTable1684171704508,
  createBotSettingsTable1684247276303,
  createPositionTable1684254023768,
  createReferralTable1684260002994,
  createDepositTable1684263858651,
  createTransactionsTable1684264969919,
];
