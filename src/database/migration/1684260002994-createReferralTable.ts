import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey,
} from "typeorm";
import {
  REFERRAL_TABLE_NAME,
  USER_TABLE_NAME,
} from "../../shared/constants/tableName";

export class createReferralTable1684260002994 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: REFERRAL_TABLE_NAME,
        columns: [
          {
            name: "id",
            type: "int",
            isGenerated: true,
            generationStrategy: "increment",
            isPrimary: true,
          },
          {
            name: "referralUser",
            type: "int",
          },
          {
            name: "userId",
            type: "numeric",
          },
          {
            name: "profit",
            type: "numeric",
          },
          {
            name: "date",
            type: "timestamp without time zone",
            default: "now()",
          },
        ],
      })
    );
    await queryRunner.createForeignKey(
      REFERRAL_TABLE_NAME,
      new TableForeignKey({
        columnNames: ["referralUser"],
        referencedColumnNames: ["id"],
        referencedTableName: USER_TABLE_NAME,
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    const table = await queryRunner.getTable(REFERRAL_TABLE_NAME);
    const foreignKey = table.foreignKeys.find(
      (fk) => fk.columnNames.indexOf("referralUser") !== -1
    );
    await queryRunner.dropForeignKey(REFERRAL_TABLE_NAME, foreignKey);
    await queryRunner.dropColumn(REFERRAL_TABLE_NAME, "referralUser");
    await queryRunner.dropTable(REFERRAL_TABLE_NAME);
  }
}
