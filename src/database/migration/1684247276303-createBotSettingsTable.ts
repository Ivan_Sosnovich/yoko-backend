import { MigrationInterface, QueryRunner, Table } from "typeorm";
import { BOT_SETTINGS_TABLE_NAME } from "../../shared/constants/tableName";

export class createBotSettingsTable1684247276303 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: BOT_SETTINGS_TABLE_NAME,
        columns: [
          {
            name: "id",
            type: "int",
            isGenerated: true,
            generationStrategy: "increment",
            isPrimary: true,
          },
          {
            name: "minBnb",
            type: "numeric",
          },
          {
            name: "minBalance",
            type: "numeric",
          },
          {
            name: "maxTraidePairs",
            type: "numeric",
          },
          {
            name: "minOrder",
            type: "numeric",
          },
          {
            name: "orderTimer",
            type: "numeric",
          },
          {
            name: "minValue",
            type: "numeric",
          },
          {
            name: "sellUp",
            type: "numeric",
          },
          {
            name: "buyDown",
            type: "numeric",
          },
          {
            name: "numAver",
            type: "boolean",
          },
          {
            name: "stepAver",
            type: "numeric",
          },
          {
            name: "maxAver",
            type: "numeric",
          },
          {
            name: "quantityAver",
            type: "numeric",
          },
          {
            name: "apiKey",
            type: "text",
          },
          {
            name: "apiAdres",
            type: "text",
          },
          {
            name: "secretKey",
            type: "text",
          },
          {
            name: "minDailyPercent",
            type: "numeric",
          },
          {
            name: "dailyPercent",
            type: "numeric",
          },
          {
            name: "progressiveMaxPairs",
            type: "boolean",
          },
          {
            name: "progressiveAverage",
            type: "boolean",
          },
          {
            name: "averagePercent",
            type: "numeric",
          },
          {
            name: "trailingStop",
            type: "boolean",
          },
          {
            name: "trailingPercent",
            type: "numeric",
          },
          {
            name: "botId",
            type: "numeric",
          },
          {
            name: "blockedPairs",
            type: "text[]",
          },
          {
            name: "allowedPairs",
            type: "text[]",
          },
        ],
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable(BOT_SETTINGS_TABLE_NAME);
  }
}
