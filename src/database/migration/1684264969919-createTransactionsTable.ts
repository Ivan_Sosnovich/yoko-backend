import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey,
  TableColumn,
} from "typeorm";
import {
  TRANSACTIONS_TABLE_NAME,
  DEPOSIT_TABLE_NAME,
} from "../../shared/constants/tableName";
export class createTransactionsTable1684264969919
  implements MigrationInterface
{
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: TRANSACTIONS_TABLE_NAME,
        columns: [
          {
            name: "id",
            type: "int",
            isGenerated: true,
            generationStrategy: "increment",
            isPrimary: true,
          },
          {
            name: "status",
            type: "text",
          },
          {
            name: "network",
            type: "text",
          },
          {
            name: "currency",
            type: "text",
          },
          {
            name: "amount",
            type: "text",
          },
          {
            name: "tx",
            type: "text",
          },
          {
            name: "sender",
            type: "text",
          },
          {
            name: "confirmations",
            type: "text",
          },
          {
            name: "createAt",
            type: "timestamp without time zone",
            default: "now()",
          },
        ],
      })
    );

    await queryRunner.addColumn(
      TRANSACTIONS_TABLE_NAME,
      new TableColumn({
        name: "orderId",
        type: "int",
        isNullable: true,
      })
    );

    await queryRunner.createForeignKey(
      TRANSACTIONS_TABLE_NAME,
      new TableForeignKey({
        columnNames: ["orderId"],
        referencedColumnNames: ["id"],
        referencedTableName: DEPOSIT_TABLE_NAME,
      })
    );

    await queryRunner.addColumn(
      DEPOSIT_TABLE_NAME,
      new TableColumn({
        name: "transactionsIds",
        type: "int",
        isNullable: true,
      })
    );

    await queryRunner.createForeignKey(
      DEPOSIT_TABLE_NAME,
      new TableForeignKey({
        columnNames: ["transactionsIds"],
        referencedColumnNames: ["id"],
        referencedTableName: TRANSACTIONS_TABLE_NAME,
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    const table = await queryRunner.getTable(TRANSACTIONS_TABLE_NAME);
    const foreignKey = table.foreignKeys.find(
      (fk) => fk.columnNames.indexOf("orderId") !== -1
    );
    await queryRunner.dropForeignKey(TRANSACTIONS_TABLE_NAME, foreignKey);
    await queryRunner.dropColumn(TRANSACTIONS_TABLE_NAME, "orderId");
    await queryRunner.dropTable(TRANSACTIONS_TABLE_NAME);
  }
}
